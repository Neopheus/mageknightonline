//simulate() needs around 0.15s
//IMPORTATNT: GACONFIG.GENERATIONS*GACONFIG.POPULATIONSIZE should not be greater than 200 (would need around 30s for reasoning)

var GACONFIG = {
  GENERATIONS: 4,
  POPULATIONSIZE: 12,
  AMOUNTSURVIVORS: null,
  MUTATIONPROBABILITY: 0.25,
  FITNESS: {
    FIGHT: 75,
    CAPTURE: 20,
    DAMAGE: 400,
    CP: 150,
    ACTIONS: 50,
    VICTORY: 1000000
  }
};
GACONFIG.AMOUNTSURVIVORS = GACONFIG.POPULATIONSIZE/2;
var simulationtimes = [];

function ga(){
  var bestIndividual = null;
  console.log("### START GA ###");
  startTimer("GA");
  var population = createInitialPopulation();
  populationFitness(population);
  for(var i=0; i<GACONFIG.GENERATIONS; i++){
    socket.emit("aiStillAlive");
    startTimer("---Iteration---"+i);
    var newGeneration = [];
    var survivors = selection(population, GACONFIG.AMOUNTSURVIVORS);
    for(var j=0; j<survivors.length; j++){
      newGeneration.push(survivors[j]);
    }
    while(newGeneration.length<GACONFIG.POPULATIONSIZE){
      var parents = selection(survivors, 2);
      var childs = crossing(parents);
      newGeneration.push(childs[0], childs[1]);
    }
    population = newGeneration;
    populationFitness(population);
    var bestIndividualInGeneration = getBestIndividual(population);
    stopTimer("---Iteration---"+i);
  }
  bestIndividual = getBestIndividual(population);
  console.log("population", population);
  console.log("bestFit", bestIndividual.fitness.raw);
  stopTimer("GA");
  console.log("avgSimTime", avg(simulationtimes));
  console.log("### END GA ###");
  return createActions(bestIndividual);
}

function createActions(individual){
  var actions = [];
  for(var i=0; i<individual.executionOrder.length; i++){
    actions.push(individual.properties[i]);
  }
  return actions;
}

function getBestIndividual(population){
  //populationFitness(population);
  population.sort(sortFitness);
  var bestIndividual = population[0];
  return bestIndividual;
}

function createInitialPopulation(){
  var population = [];
  for(var i=0; i<GACONFIG.POPULATIONSIZE; i++){
    population.push(createRandomIndividual());
  }
  return population;
}

function createRandomIndividual(){
  var individual = createEmptyIndividual();
  var index=0;
  for(var warrior in GAMESTATE["armies"][PLAYER.id]){
    individual.properties.push(createRandomProperty(PLAYER.id, warrior));
    individual.executionOrder.push(index);
    index++;
  }
  shuffleArray(individual.executionOrder);
  return individual;
}

function createEmptyIndividual(){
  var emptyIndividual = {
    properties: [],
    executionOrder: [],
    fitness: {
      raw: null,
      normalized: null,
      accumulated: null
    }
  };
  return emptyIndividual;
}

function createRandomProperty(owner, index){
  var property = {
    owner: owner,
    index: index,
    script: getRandomScript()
  };
  return property;
}

function simulate(actionsToSimulate){
  var last = Date.now();
  var simulatedGS;
  saveGamestate();
  SIMULATING = true;
  actions = actionsToSimulate;
  ai();
  simulatedGS = cloneObject(GAMESTATE);
  SIMULATING = false;
  restoreGamestate();
  simulationtimes.push(Date.now()-last);
  return simulatedGS;
}

function calculateFitness(gamestate, PRINT){
  if(PRINT) console.log("--- calculateFitness() ---");
  if(PRINT) console.log("from player sight");
  var ownFitness = _calculateFitness(gamestate, PLAYER, OPPONENT, PRINT);
  if(PRINT) console.log("from opponent sight");
  var oppFitness = _calculateFitness(gamestate, OPPONENT, PLAYER, PRINT);
  oppFitness = (oppFitness===0) ? 1 : oppFitness;
  var fitness = ownFitness/oppFitness;
  if(PRINT) console.log("Fitness (own/opp): ", fitness);
  if(PRINT) console.log("--------------------------");
  return fitness;
}

function _calculateFitness(gamestate, player, opponent, PRINT){
  var fitness = 0;
  if(gamestate.gameover){
    if(gamestate.winner==="player.id") fitness = GACONFIG.FITNESS.VICTORY;
  } else {
    //fightFitness: calculates the chance to hit and get hit for every warrior
    var fightFitness = 0;
    for(var warrior in gamestate["armies"][player.id]) {
      warrior = gamestate["armies"][player.id][warrior];
      var warFitness = 0;
      for(var enemy in gamestate["armies"][opponent.id]){
        enemy = gamestate["armies"][opponent.id][enemy];
        var dist = getDistance(warrior, enemy);
        var chanceToHit = 0;
        if(dist<=inchToPixels(warrior.activeState.movementrange)+warrior.baseradius+enemy.baseradius){
          var contact = (isInList(warrior, enemy.baseContacts)) ? 2 : 1;
          var action = 2-warrior.actions;
          var attackbonus = (warrior.Attacktype==="sword") ? warrior.attackbonus : 0;
          var diceProb = getDiceProbability(enemy.activeState.defence-(warrior.activeState.attack+attackbonus));
          chanceToHit+=(contact*action*diceProb);
        }
        if(checkInRange(warrior, enemy) && checkFreeSight(warrior, enemy) && !isInList(enemy, warrior.baseContacts)){
          var action = 2-warrior.actions;
          var attackbonus = (warrior.Attacktype!="sword") ? warrior.attackbonus : 0;
          var diceProb = getDiceProbability(enemy.activeState.defence-(warrior.activeState.attack+attackbonus));
          chanceToHit+=(action*diceProb);
        }
        var chanceToGetHit = 0;
        if(dist<=inchToPixels(enemy.activeState.movementrange)+warrior.baseradius+enemy.baseradius){
          var contact = (isInList(enemy, warrior.baseContacts)) ? 2 : 1;
          var action = 2-enemy.actions;
          var attackbonus = (enemy.Attacktype==="sword") ? enemy.attackbonus : 0;
          var diceProb = getDiceProbability(warrior.activeState.defence-(enemy.activeState.attack+attackbonus));
          chanceToGetHit+=(contact*action*diceProb);
        }
        if(checkInRange(enemy, warrior) && checkFreeSight(enemy, warrior) && !isInList(warrior, enemy.baseContacts)){
          var action = 2-enemy.actions;
          var attackbonus = (enemy.Attacktype!="sword") ? enemy.attackbonus : 0;
          var diceProb = getDiceProbability(warrior.activeState.defence-(enemy.activeState.attack+attackbonus));
          chanceToGetHit+=(action*diceProb);
        }
        chanceToGetHit = (chanceToGetHit===0) ? 1 : chanceToGetHit;
        warFitness+=GACONFIG.FITNESS.FIGHT*(enemy.costs/warrior.costs)*(chanceToHit/chanceToGetHit);
      }
      fightFitness+=warFitness;
    }
    if(PRINT) console.log("fightFitness", fightFitness);
    fitness+=fightFitness;

    //captureFitness: calculates the wighted distances from evry warrior to all CPs
    var capFitness = GACONFIG.FITNESS.CAPTURE;
    var ownDist = 0;
    for(var warrior in gamestate["armies"][player.id]){
      warrior = gamestate["armies"][player.id][warrior];
      var cp = getNextCPtoCapture(warrior, gamestate);
      if(cp){
        var wight = (cp.owner===opponent.id) ? 0.5 : 1;
        var dist = distance(warrior.position, cp.position);
        ownDist+=dist*wight;
      }
    }
    ownDist/=Object.keys(gamestate["armies"][player.id]).length;
    var oppDist = 0;
    for(var enemy in gamestate["armies"][opponent.id]){
      enemy = gamestate["armies"][opponent.id][enemy];
      var cp = getNextCPtoCapture(enemy, gamestate);
      if(cp){
        var wight = (cp.owner===player.id) ? 0.5 : 1;
        var dist = distance(enemy.position, cp.position);
        oppDist+=dist*wight;
      }
    }
    oppDist/=Object.keys(gamestate["armies"][opponent.id]).length;
    oppDist = (oppDist===0) ? 1 : oppDist;
    capFitness*=(oppDist/ownDist);
    if(PRINT) console.log("capFitness", capFitness);
    fitness+=capFitness

    //damageFitness
    var dmgFitness = GACONFIG.FITNESS.DAMAGE;
    var oppDmg = 0;
    for (var warrior in gamestate["armies"][opponent.id]) {
      warrior = gamestate["armies"][opponent.id][warrior];
      oppDmg+=(warrior.damageTaken/(12-warrior.life))*(warrior.costs/opponent.ARMYSIZE);
    }
    var ownDmg = 0;
    for (var warrior in gamestate["armies"][player.id]) {
      warrior = gamestate["armies"][player.id][warrior];
      ownDmg+=(warrior.damageTaken/(12-warrior.life))*(warrior.costs/player.ARMYSIZE);
    }
    ownDmg = (ownDmg===0) ? 1 : ownDmg;
    dmgFitness*=(oppDmg/ownDmg);
    if(PRINT) console.log("dmgFitness", dmgFitness);
    fitness+=dmgFitness;

    //cpFitness
    var cpFitness = GACONFIG.FITNESS.CP;
    if(player.id===gamestate.allCpHold.from){
      cpFitness*= 1+(gamestate.allCpHold.rounds/(GAME.ROUNDSTOWIN*2));
    } else if(opponent.id===gamestate.allCpHold.from){
      cpFitness*= 1-(gamestate.allCpHold.rounds/(GAME.ROUNDSTOWIN*2));
    }
    var ownCP = 0;
    var oppCP = 0;
    for(var cp in gamestate["controlpoints"]){
      cp = gamestate["controlpoints"][cp];
      if(cp.owner===player.id){
        ownCP++;
      } else if(cp.owner===opponent.id){
        oppCP++;
      }
    }
    oppCP = (oppCP===0) ? 1 : oppCP;
    cpFitness*=(ownCP/oppCP);
    if(PRINT) console.log("cpFitness", cpFitness);
    fitness+=cpFitness;

    //actFitness
    var actFitness = GACONFIG.FITNESS.ACTIONS;
    actFitness*= (player===gamestate.activePlayer) ? gamestate.actions : 0;
    if(PRINT) console.log("actFitness", actFitness);
    fitness+=actFitness;
  }

  if(PRINT) console.log("fitness", fitness);
  return fitness;
}

function populationFitness(population){
  for(var i=0; i<population.length; i++){
    if(population[i].fitness.raw===null){
      var individual = cloneIndividual(population[i]);
      var simulatedGS = simulate(createActions(individual));
      population[i].fitness.raw = calculateFitness(simulatedGS);
    }
  }
}

function prepareForSelection(population){
  population.sort(sortFitness);
  normalizeFitness(population);
  accumulateFitness(population);
}

function selection(population, amount){
  prepareForSelection(population);
  var survivor = [];
  for(var i=0; i<amount; i++){
    var rand = random();
    var lastFittingIndex = 0;
    for(var j=0; j<population.length; j++){
      if(population[j].fitness.accumulated<rand){
        lastFittingIndex = j;
      } else {
        break;
      }
    }
    survivor.push(cloneIndividual(population[lastFittingIndex]));
  }
  return survivor;
}

function crossing(parents){
  var length = parents[0].properties.length;
  var crossPoint = length/2;
  var childs = [
    createEmptyIndividual(),
    createEmptyIndividual()
  ];
  for(var i=0; i<length; i++){
    if(i<crossPoint){
      childs[0].properties.push(cloneProperty(parents[0].properties[i]));
      childs[1].properties.push(cloneProperty(parents[1].properties[i]));
    } else {
      childs[0].properties.push(cloneProperty(parents[1].properties[i]));
      childs[1].properties.push(cloneProperty(parents[0].properties[i]));
    }
  }
  childs[0].executionOrder = cloneExecutionOrder(parents[0].executionOrder);
  childs[1].executionOrder = cloneExecutionOrder(parents[1].executionOrder);
  mutation(childs[0]);
  mutation(childs[1]);
  return childs;
}

function mutation(individual){
  if(random()<=GACONFIG.MUTATIONPROBABILITY){
    if(random()<0.75 || individual.properties.length===1) {
      //mutation with script exchange
      var randIndex = Math.floor(random()*individual.properties.length);
      var randomScript;
      var same = true;
      while(same){
        randomScript = getRandomScript();
        if(randomScript!=individual.properties[randIndex].script) same = false;
      }
      individual.properties[randIndex].script = randomScript;
    } else {
      //mutation with swapping two properties in the executionOrder
      var a, b;
      var same = true;
      while(same){
        a = Math.floor(random()*individual.executionOrder.length);
        b = Math.floor(random()*individual.executionOrder.length);
        if(a!=b) same = false;
      }
      var tmp = cloneProperty(individual.executionOrder[b]);
      individual.executionOrder[b] = cloneProperty(individual.executionOrder[a]);
      individual.executionOrder[a] = tmp;
    }
  }
}

function accumulateFitness(population){
  var accumulatedFit = 0;
  for(var i=0; i<population.length; i++){
    accumulatedFit+=population[i].fitness.normalized;
    population[i].fitness.accumulated = accumulatedFit;
  }
}

function normalizeFitness(population){
  var overallFit = overallFitness(population);
  for(var i=0; i<population.length; i++){
    population[i].fitness.normalized = population[i].fitness.raw/overallFit;
  }
}

function overallFitness(population){
  var overallFit = 0;
  for(var i=0; i<population.length; i++) {
    overallFit+=population[i].fitness.raw;
  }
  return overallFit;
}

function sortFitness(a, b){
  return b.fitness.raw-a.fitness.raw;
}

function shuffleArray(a){
  var j, x, i;
  for (i = a.length; i; i--) {
      j = Math.floor(random() * i);
      x = a[i - 1];
      a[i - 1] = a[j];
      a[j] = x;
  }
}

function cloneIndividual(individual){
  var clone = {
    properties: cloneProperties(individual.properties),
    executionOrder: cloneExecutionOrder(individual.executionOrder),
    fitness: {
      raw: individual.fitness.raw,
      normalized: individual.fitness.normalized,
      accumulated: individual.fitness.accumulated,
    }
  };
  return clone;
}

function cloneExecutionOrder(order){
  var clone = [];
  for(var i=0; i<order.length; i++){
    clone.push(order[i]);
  }
  return clone;
}

function cloneProperties(properties){
  var clone = [];
  for(var i=0; i<properties.length; i++){
    clone.push(cloneProperty(properties[i]));
  }
  return clone;
}

function cloneProperty(property){
  var clone = {
    owner: property.owner,
    index: property.index,
    script: property.script,
  };
  return clone;
}
