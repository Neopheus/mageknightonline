var imgTable = new Image();
imgTable.src = "imgs/table.png";

var iconShoe = new Image();
iconShoe.src = "imgs/iconShoe.png";
iconShoe.title = "Movement: Shoe";
iconShoe.alt = "Movement: Shoe";

var iconHorseshoe = new Image();
iconHorseshoe.src = "imgs/iconHorseshoe.png";
iconHorseshoe.title = "Movement: Horseshoe";
iconHorseshoe.alt = "Movement: Horseshoe";

var iconWing = new Image();
iconWing.src = "imgs/iconWing.png";
iconWing.title = "Movement: Wing";
iconWing.alt = "Movement: Wing";

var iconWave = new Image();
iconWave.src = "imgs/iconWave.png";
iconWave.title = "Movement: Wave";
iconWave.alt = "Movement: Wave";

var iconSword = new Image();
iconSword.src = "imgs/iconSword.png";
iconSword.title = "Attack: Sword";
iconSword.alt = "Attack: Sword";

var iconWand = new Image();
iconWand.src = "imgs/iconWand.png";
iconWand.title = "Attack: Wand";
iconWand.alt = "Attack: Wand";

var iconBow = new Image();
iconBow.src = "imgs/iconBow.png";
iconBow.title = "Attack: Bow";
iconBow.alt = "Attack: Bow";

var iconShield = new Image();
iconShield.src = "imgs/iconShield.png";
iconShield.title = "Defence: Shield";
iconShield.alt = "Defence: Shield";

var iconMagicImune = new Image();
iconMagicImune.src = "imgs/iconMagicImune.png";
iconMagicImune.title = "Defence: Magic imune";
iconMagicImune.alt = "Defence: Magic imune";

var iconDamage = new Image();
iconDamage.src = "imgs/iconDamage.png";
iconDamage.title = "Damage: Normal";
iconDamage.alt = "Damage: Normal";

var iconGolem = new Image();
iconGolem.src = "imgs/iconGolem.png";
iconGolem.title = "Damage: Golem";
iconGolem.alt = "Damage: Golem";

var iconMarker = new Image();
iconMarker.src = "imgs/iconMarker.png";
iconMarker.title = "Marker";
iconMarker.alt = "Marker";

ICON = {
	MOVEMENT: function(warrior){
		if(warrior.movementtype=="shoe") return iconShoe;
		if(warrior.movementtype=="horseshoe") return iconHorseshoe;
		if(warrior.movementtype=="wing") return iconWing;
		if(warrior.movementtype=="wave") return iconWave;
	},
	ATTACK: function(warrior){
		if(warrior.attacktype=="sword") return iconSword;
		if(warrior.attacktype=="wand") return iconWand;
		if(warrior.attacktype=="bow") return iconBow;
	},
	DEFENCE: function(warrior){
		if(warrior.defensetype=="normal") return iconShield;
		if(warrior.defensetype=="magic") return iconMagicImune;
	},
	DAMAGE: function(warrior){
		if(warrior.damagetype=="normal") return iconDamage;
		if(warrior.damagetype=="golem") return iconGolem;
	},
	MARKER: iconMarker
}
