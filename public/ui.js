var msgCount = 0;
var lastSearchTerm = "";

function setSelectArmyHeights(){
	var windowHeight = $(document).height();
	var toolbarHeight = $("#armySelectToolbar").outerHeight(true);
	var searchbarsHeight = 2*$("#armySearch").outerHeight(true);
	var messageBoxHeight = $("#messageBox").outerHeight(true);
	var calculatedHeight = ((windowHeight - toolbarHeight - searchbarsHeight - messageBoxHeight) / 2) - 15;
	$("#armyTableDiv, #warriorsTableDiv").height(calculatedHeight);
}

function newMessage(msg){
	if(!SIMULATING){
		id = "msg"+msgCount;
		msgCount++;
		addMessage(msg, id);
		markMessage(id);
		showMessage(id);
		scrollDown();
		unmarkMessage(id);
	}
}

function addMessage(msg, id){
	$("#messageList").append(
		$("<li id='"+id+"'></li>").append("\u2022 ", msg)
	);
}

function markMessage(id){
	$("#"+id).attr("class", "markedMsg");
}

function showMessage(id){
	$("#"+id).fadeIn("slow");
}

function scrollDown(){
	$("#messageBox").scrollTop($("#messageBox")[0].scrollHeight);
}

function unmarkMessage(id){
	setTimeout(function(){
		$("#"+id).attr("class", "unmarkedMsg");
	}, 3*1000);
}

function emptyMessageList(){
	$("#messageList").empty();
	msgCount = 0;
}

function addNewGameToList(game){
	if(game.id!=LOBBY){
		var name = game.name;
		var joinedPlayers = game.joinedPlayers;
		var pw = null;
		var joinButton = $("<button></button>").append("Join!");
		if(game.pw){
			pw = "Yes";
			joinButton.attr({type: "button", id: game.id, class: "joinGameWithPW"});
		} else {
			pw = "No";
			joinButton.attr({type: "button", id: game.id, class: "joinGameWithoutPW"});
		}
		if(game.joinedPlayers>=2){
			joinedPlayers = "FULL";
			joinButton.prop("disabled", true);
		}
		$("#listedGames").append(
			$("<tr></tr>").append(
				$("<td></td>").append("<i>"+name+"</i>"),
				$("<td></td>").append("<i>"+joinedPlayers+"</i>"),
				$("<td></td>").append("<i>"+pw+"</i>"),
				$("<td></td>").append(joinButton)
			)
		);
	}
}

function addWarriorToList(warrior){
	var addButton = $("<button></button>").append("Add!");
	addButton.attr({type: "button", id: warrior.id, class: "addWarrior"});
	$("#listedWarriors").append(
		$("<tr></tr>").append(
			$("<td></td>").append(warrior.id),
			$("<td></td>").append(warrior.name),
			$("<td></td>").append(warrior.faction),
			$("<td></td>").append(warrior.subfaction),
			$("<td></td>").append(warrior.rank),
			$("<td></td>").append(warrior.costs),
			$("<td class='base' warriorId='"+warrior.id+"'></td>").append("(●)"),
			$("<td></td>").append(addButton)
		)
	);
}

function addWarriorToArmyList(warrior){
	var removeButton = $("<button></button>").append("Remove!");
	removeButton.attr({type: "button", id: warrior.id, class: "removeWarrior"});
	$("#listedArmy").append(
		$("<tr></tr>").append(
			$("<td></td>").append(warrior.id),
			$("<td></td>").append(warrior.name),
			$("<td></td>").append(warrior.faction),
			$("<td></td>").append(warrior.subfaction),
			$("<td></td>").append(warrior.rank),
			$("<td></td>").append(warrior.costs),
			$("<td class='base' warriorId='"+warrior.id+"'></td>").append("(●)"),
			$("<td></td>").append(removeButton)
		)
	);
}

function updateArmyList(){
	$("#listedArmy").empty();
	for(var i=0; i<ARMY.length; i++){
		addWarriorToArmyList(warriorDB[ARMY[i]]);
	}
	$("#armysize").val(getArmySize());
	if(ARMY.length!=0){
		$("#ready").show();
	} else {
		$("#ready").hide();
	}
}

function updateUI(){
	//buttons
	updateEndTurnButton();
	updateAbortSpinsButton();
	updateActionButtons();
	//turninfo
	updateActiveUser();
	updateElapsedTurns();
	updateExecutedActions();
	//warrioroverview
	updateWarriorOverview();
	//minimap
	updateMinimap();
}

function updateEndTurnButton(){
	if(yourTurn() && !freeRotation && !placeWarriors){
		$("#endTurnButton").removeClass("bDisabled");
	} else {
		$("#endTurnButton").addClass("bDisabled");
	}
}

function updateAbortSpinsButton(){
	if(freeRotation && !yourTurn()){
		$("#abortSpinsButton").show();
	} else {
		$("#abortSpinsButton").hide();
	}
}

function updateActionButtons(){
	if(yourTurn() && GAMESTATE.executedActions<3 && !placeWarriors){
		$("#giveMovementAction").removeClass("bDisabled");
		$("#giveAttackAction").removeClass("bDisabled");
	} else {
		$("#giveMovementAction").addClass("bDisabled");
		$("#giveAttackAction").addClass("bDisabled");
	}
}

function updateActiveUser(){
	if(yourTurn()){
		$("#activePlayer").html("Active player: <font color='"+COLOR.ALLY+"'>"+PLAYER.name+" (You)<font>");
	} else {
		$("#activePlayer").html("Active player: <font color='"+COLOR.FOE+"'>"+OPPONENT.name+" (Opponent)<font>");
	}
}

function updateElapsedTurns(){
	$("#elapsedTurns").html("Elapsed Turns: "+GAMESTATE.turn);
}

function updateExecutedActions(){
	var actionsLeft = 3-GAMESTATE.executedActions;
	if(actionsLeft!=0){
		$("#actionsLeft").html("Actions left: <font color='"+COLOR.ALLY+"'>"+actionsLeft+"<font>");
	} else {
		$("#actionsLeft").html("Actions left: <font color='"+COLOR.FOE+"'>"+actionsLeft+"<font>");
	}
}

function updateMinimap(){
	var mmImg = $("#minimapImg");
	var imgSrc = screenshot();
	mmImg.attr("src", imgSrc);
}

function hideWarriorTooltip(){
	$("#warriorTooltip").empty();
	$("#warriorTooltip").hide();
}

function showWarriorTooltip(warrior, mouse){
	var tooltip = $("#warriorTooltip");
	tooltip.attr({align: "center"});
	if((mouse.pageX+15+tooltip.width())>=window.innerWidth){
		tooltip.css("left", mouse.pageX-15-tooltip.width());
		tooltip.css("top", mouse.pageY-(tooltip.height()/2));
	} else {
		tooltip.css("left", mouse.pageX+15);
		tooltip.css("top", mouse.pageY-(tooltip.height()/2));
	}
	if(lastWarriorAtPos==null || !sameWarrior(warrior, lastWarriorAtPos)){
		if(yourWarrior(warrior)){
			tooltip.css("background-color", COLOR.ALLY);
		} else {
			tooltip.css("background-color", COLOR.FOE);
		}
		tooltip.empty();
		tooltip.append(
			$("<p></p>").append(
				$("<b></b>").append(warrior.name)
			),
			$("<table align='center'></table>").append(
				$("<tr></tr>").append(
					$("<td align='center'></td>").append(ICON.MOVEMENT(warrior)),
					$("<td align='center'></td>").append(newValue(warrior.activeState.movementrange, warrior.activeState.movementboni))
				),
				$("<tr></tr>").append(
					$("<td align='center'></td>").append(ICON.ATTACK(warrior)),
					$("<td align='center'></td>").append(newValue(warrior.activeState.attack, warrior.activeState.attackboni))
				),
				$("<tr></tr>").append(
					$("<td align='center'></td>").append(ICON.DEFENCE(warrior)),
					$("<td align='center'></td>").append(newValue(warrior.activeState.defence, warrior.activeState.defenceboni))
				),
				$("<tr></tr>").append(
					$("<td align='center'></td>").append(ICON.DAMAGE(warrior)),
					$("<td align='center'></td>").append(newValue(warrior.activeState.damage, warrior.activeState.damageboni))
				),
				$("<tr></tr>").append(
					$("<td align='center'></td>").append(ICON.MARKER),
					$("<td align='center'></td>").append(warrior.activeState.marker)
				)
			)
		);
		tooltip.show();
		tooltip.find(".value").each(function(){
			$(this).width($(this).height());
		});
	} else {
		tooltip.show();
	}
}

function newValue(x, abilities) {
	var container = $("<div></div>");
	container.css("display", "flex");
	container.css("justify-content","center");

	if (abilities.length > 0) {
		var value = $("<div class='value'></div>");
		var color = abilities[0].substring(1, abilities[0].length - 1);
		value.append(x);
		value.css("text-align", "center");
		value.css("background-color", ABILITYCOLOR[color]["background"]);
		value.css("color", ABILITYCOLOR[color]["text"]);
		value.css("border", "1px solid black");
		if (abilities[0].charAt(0) == "(") value.css("border-radius", "20px");
		container.append(value);

		if (abilities.length > 1) {
			var additional = $("<div class='value'></div>");
			var color = abilities[1].substring(1, abilities[1].length - 1);
			additional.append(x);
			value.css("float", "left");
			additional.css("float", "left");
			additional.css("margin-left", "3px");
			additional.css("text-align", "center");
			additional.css("background-color", ABILITYCOLOR[color]["background"]);
			additional.css("color", ABILITYCOLOR[color]["background"]);
			additional.css("border", "1px solid black");
			if (abilities[1].charAt(0) == "(") additional.css("border-radius", "20px");
			container.append(additional);
		}
	} else {
		container.append(x);
	}

	return container;
}

function updateWarriorOverview(){
	hideWarriorOverview();
	showWarriorOverview();
}

function hideWarriorOverview(){
	$("#warriorOverviews").empty();
	$("#warriorOverviews").hide();
}

function showWarriorOverview(){
	if(selectedWarrior!=null){
		$("#warriorOverviews").empty();
		addOverview(selectedWarrior, 0);
		for(var i=0; i<selectedWarrior.baseContacts.length; i++){
			addOverview(getWarrior(selectedWarrior.baseContacts[i].owner, selectedWarrior.baseContacts[i].index), i+1, "close")
		}
		for(var i=0; i<selectedWarrior.rangeTargets.length; i++){
			addOverview(getWarrior(selectedWarrior.rangeTargets[i].owner, selectedWarrior.rangeTargets[i].index), i+1+selectedWarrior.baseContacts.length, "range")
		}
		$("#warriorOverviews").show();
	}
}

function addOverview(warrior, pos, distance){
	var overview = $("<div></div>");
	if(yourWarrior(warrior)){
		var overviewClass = "allyOverview";
	} else {
		var overviewClass = "foeOverview";
	}
	overview.attr({class: overviewClass, align: "center", style: "right: "+(pos*overviewWidth)+"px;"});
	overview.append(
		$("<p></p>").append(
			$("<b></b>").append(warrior.name)
		)
	);
	overview.append(
		$("<table></table>").append(
			$("<caption></caption>").append("Base values:"),
			$("<tr></tr>").append(
				$("<td></td>").append("Movementtype"),
				$("<td></td>").append(warrior.movementtype)
			),
			$("<tr></tr>").append(
				$("<td></td>").append("Attacktype"),
				$("<td></td>").append(warrior.attacktype)
			),
			$("<tr></tr>").append(
				$("<td></td>").append("Attackbonus"),
				$("<td></td>").append(warrior.attackbonus)
			),
			$("<tr></tr>").append(
				$("<td></td>").append("Defensetype"),
				$("<td></td>").append(warrior.defensetype)
			),
			$("<tr></tr>").append(
				$("<td></td>").append("Damagetype"),
				$("<td></td>").append(warrior.damagetype)
			),
			$("<tr></tr>").append(
				$("<td></td>").append("Range"),
				$("<td></td>").append(warrior.range)
			),
			$("<tr></tr>").append(
				$("<td></td>").append("Rangeattacks"),
				$("<td></td>").append(warrior.rangeattacks)
			),
			$("<tr></tr>").append(
				$("<td></td>").append("Rangedamage"),
				$("<td></td>").append(warrior.rangedamage)
			)
		), $("<br>")
	);
	overview.append(
		$("<table></table>").append(
			$("<caption></caption>").append("Status:"),
			$("<tr></tr>").append(
				$("<td></td>").append("MaxHP"),
				$("<td></td>").append(warrior.life)
			),
			$("<tr></tr>").append(
				$("<td></td>").append("DamageTaken"),
				$("<td></td>").append(warrior.damageTaken)
			),
			$("<tr></tr>").append(
				$("<td></td>").append("Actions"),
				$("<td></td>").append(warrior.actions)
			),
			$("<tr></tr>").append(
				$("<td></td>").append("gotAction"),
				$("<td></td>").append(warrior.gotAction.toString())
			)
		), $("<br>")
	);
	overview.append(
		$("<table></table>").append(
			$("<caption></caption>").append("Active stats:"),
			$("<tr></tr>").append(
				$("<td></td>").append("Movement"),
				$("<td></td>").append(warrior.activeState.movementrange)
			),
			$("<tr></tr>").append(
				$("<td></td>").append("Attack"),
				$("<td></td>").append(warrior.activeState.attack)
			),
			$("<tr></tr>").append(
				$("<td></td>").append("Defence"),
				$("<td></td>").append(warrior.activeState.defence)
			),
			$("<tr></tr>").append(
				$("<td></td>").append("Damage"),
				$("<td></td>").append(warrior.activeState.damage)
			),
			$("<tr></tr>").append(
				$("<td></td>").append("Marker"),
				$("<td></td>").append(warrior.activeState.marker)
			)
		), $("<br>")
	);
	if(yourWarrior(selectedWarrior) && pos>0){
		if(opponentsWarrior(warrior) ){
			var attackButton = $("<button></button>").append("Attack");
			if(distance=="close"){
				attackButton.attr({class: "attackClose", owner: warrior.owner, index: warrior.index, type: "button"});
			} else if(distance=="range"){
				attackButton.attr({class: "attackRange", owner: warrior.owner, index: warrior.index, type: "button"});
			}
			if(!isActionPossible(selectedWarrior) || !checkInSight(selectedWarrior, warrior, "position")){
				attackButton.prop("disabled", true);
			}
			overview.append(attackButton);
		} else if(yourWarrior(warrior)){
			//TODO add check for possibility of healing
			var healButton = $("<button></button>").append("Heal/Repair");
			healButton.attr({class: "heal", value: warrior.owner, id: warrior.index, type: "button"});
			if(!isActionPossible(selectedWarrior) || !checkInSight(selectedWarrior, warrior, "position")){
				healButton.prop("disabled", true);
			}
			//overview.append(healButton);
		}
	} else if(yourWarrior(selectedWarrior) && pos==0 && contactWithOpponent(selectedWarrior)){
		var breakFreeButton = $("<button></button>").append("Break free!");
		breakFreeButton.attr({id: "breakFree", type: "button"});
		if(!isActionPossible(selectedWarrior)){
			breakFreeButton.prop("disabled", true);
		}
		overview.append(breakFreeButton);
	}
	$("#warriorOverviews").prepend(overview);
}

function showBase(warriorId){
	var container = $("#diskContainer");
	if(warriorId!=container.attr("warriorId")){
		var disk = createDisk(warriorDB[warriorId]);
		var container = $("<div></div>");
		container.click(function(){
			$(this).hide();
		});
		container.attr({
			"id": "diskContainer",
			"warriorId": warriorId
		});
		container.css({
			"position": "fixed",
			"top": "0",
			"left": "0",
			"width": window.innerWidth,
			"height": window.innerHeight,
			"background-color": "rgba(0,0,0,0.5)",
			"z-index": "888"
		});
		disk.css({
			"top": container.height()/2-disk.height()/2,
			"left": container.width()/2-disk.width()/2,
		});
		container.append(disk);
		$("#armySelectUi").prepend(container);
	} else {
		$("#diskContainer").show();
	}
}

function createDisk(warrior){
	//create the disk
	var disk = $("<div id='disk'></div>");
	disk.css({
		"position": "absolute",
		"width": "225px",
		"height": "225px",
		"background-color": "white",
		"border-radius": "225px",
		"border": "5px solid black"
	});
	//scroll eventhander on the disk
	disk[0].onwheel = function(e){
		var klix = 1*Math.sign(e.deltaY);
		var disk = $(this);
		//loop over all states on disk and rotate it
		disk.find(".state").each(function(){
			var state = $(this);
			var currentDeg = parseInt(state.attr("degree"));
			var newDeg = currentDeg+(30*klix);
			state.css("transform", "rotate("+newDeg+"deg)");
			state.attr("degree", newDeg);
		});
	};
	//create 12 states on disk
	for(var i=0; i<12; i++){
		var deg = i*(-30);
		state = warrior.state[i];
		var stateDiv = $("<div></div>");
		stateDiv.attr({
			"class": "state",
			"degree": deg
		}).css({
			"display": "grid",
			"position": "absolute",
			"top": "159px",
			"left": "96px",
			"background-color": "none",
			"border-left": "1px dotted black",
			"transform-origin": "35% -75%",
			"transform": "rotate("+deg+"deg)"
		});
		//fill state with values
		for(var attribute in ATTRIBUTES){
			var value, bonus, ability, color;
			var createdAttributes = [];
			var attributeDiv = $("<div></div>");
			createdAttributes.push(attributeDiv);
			attributeDiv.css({
				"text-align": "center",
				"width": "20px",
				"height": "20px",
				"margin": "0px 1px 1px 1px",
				"padding": "0px 0x 0px 0px",
				"font": "15px calibri, sans-serif",
				"class": attribute,
				"grid-area": attribute
			});
			value = (state) ? state[attribute] : (attribute!="damage") ? "💀" : "";
			attributeDiv.append(value);
			bonus = ATTRBONUS[attribute];
			if(state && state[bonus].length>=1){
				ability = state[bonus][0][0];
				attributeDiv.css("border-radius", (ability==="[") ? "0px" : "100px");
				color = state[bonus][0].slice(1, -1);
				attributeDiv.css("background-color", ABILITYCOLOR[color].background);
				attributeDiv.css("color", ABILITYCOLOR[color].text);
				if(state[bonus].length>=2){
					var additionalDiv = $("<div></div>");
					createdAttributes.push(additionalDiv);
					additionalDiv.css({
						"text-align": "center",
						"width": "14px",
						"height": "14px",
						"margin": "0px 1px 1px 1px",
						"padding": "0px 0x 0px 0px",
						"font": "10px calibri, sans-serif",
						"class": attribute,
						"grid-area": "additional",
						"justify-self": "center",
						"align-self": "center"
					});
					additionalDiv.append(ATTRDIRECTION[attribute]);
					ability = state[bonus][1][0];
					additionalDiv.css("border-radius", (ability==="[") ? "0px" : "100px");
					color = state[bonus][1].slice(1, -1);
					additionalDiv.css("background-color", ABILITYCOLOR[color].background);
					additionalDiv.css("color", ABILITYCOLOR[color].text);
				}
			}
			//append value to state
			stateDiv.append(createdAttributes);
		}
		//append state to disk
		disk.append(stateDiv);
	}
	//return the whole disk
	return disk;
}
