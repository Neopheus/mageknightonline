var DIALOGCONFIG = {
	"BACKGROUND_COLOR": "rgba(0,0,0,0.5)",
	"BANNER_COLOR": "rgba(255,255,255,1)",
	"FONT": "bold 15px calibri, sans-serif",
	"FONT_COLOR": "rgba(0,0,0,1)",
};

function prompt(msg, fOk, pw){
	var background = createBackground();
	var banner = createBanner();
	var content = createContent();
	var text = createText(msg);
	var input = createField(function(key){
		if(key.which===13){
			var value = $(this).val();
			$(this).parents().eq(2).remove();
			fOk(value);
		}
	},pw);
	var bOk = createButton("Ok", function(){
		var selector = "input[type=text],input[type=password]";
		var value = $(this).siblings(selector).val();
		$(this).parents().eq(2).remove();
		fOk(value);
	});
	var bCancle = createButton("Cancle", function(){
		$(this).parents().eq(2).remove();
	});

	//append all
	$("body").prepend(
		background.append(
			banner.append(
				content.append(
					text,
					input,
					bOk,
					bCancle
				)
			)
		)
	);
	$("input").first().focus();
}

function confirm(msg, fYes, fNo){
	var background = createBackground();
	var banner = createBanner();
	var content = createContent();
	var text = createText(msg);
	var bYes = createButton("Yes", function(){
		$(this).parents().eq(2).remove();
		fYes();
	});
	var bNo = createButton("No", function(){
		$(this).parents().eq(2).remove();
		fNo();
	});

	//append all
	$("body").prepend(
		background.append(
			banner.append(
				content.append(
					text,
					bYes,
					bNo
				)
			)
		)
	);
	$("input").first().focus();
}

function alert(msg){
	var background = createBackground();
	var banner = createBanner();
	var content = createContent();
	var text = createText(msg);
	var bOk = createButton("Ok", function(){
		$(this).parents().eq(2).remove();
	});

	//append all
	$("body").prepend(
		background.append(
			banner.append(
				content.append(
					text,
					bOk
				)
			)
		)
	);
	$("input").first().focus();
}

function createBackground(){
	var background = $("<div></div>");
	background.css({
		"display": "table",
		"position": "fixed",
		"overflow": "hidden",
		"top": 0,
		"left": 0,
		"width": "100%",
		"height": "100%",
		"background": DIALOGCONFIG.BACKGROUND_COLOR,
		"z-index": 999
	});
	return background;
}

function createBanner(){
	var banner = $("<div></div>");
	banner.css({
		"display": "table-cell",
		"vertical-align": "middle",
		"position": "absolute",
		"top": "45%",
		"width": "100%",
		"background": DIALOGCONFIG.BANNER_COLOR
	});
	return banner;
}

function createContent(){
	var content = $("<div></div>");
	content.css({
   	"position": "relative",
   	"top": "-50%",
   	"width": "75%",
   	"margin": "auto auto",
		"text-align": "center"
	});
	return content;
}

function createText(msg){
	if(Array.isArray(msg)) {msg = "[Array]";}
	else if(typeof msg === "object" && msg!==null) {msg = "{Object}";}
	var text = $("<div></div>");
	text.css({
		"margin-top": 20,
		"margin-bottom": 5,
		"font": DIALOGCONFIG.FONT,
		"color": DIALOGCONFIG.FONT_COLOR
	});
	text.append(msg);
	return text;
}

function createButton(value, clickFunction){
	var button = $("<input>");
	button.attr({
		"type": "button",
		"value": value
	}).css({
		"margin-top": 5,
		"margin-bottom": 20
	});
	button.click(clickFunction);
	return button;
}

function createField(fKey, pw){
	var field = $("<input>");
	var type = (pw) ? "password" : "text";
	field.attr({
		"type": type
	}).css({
		"margin-top": 5,
		"margin-bottom": 20
	});
	field.on("keyup", fKey);
	return field;
}
