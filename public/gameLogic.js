//#################
//### VARIABLES ###
//#################

var SAVEDSEED = null;
var warriorDB = null;
var CAMERA = {
	x: 0,
	oldX: 0,
	y: 0,
	oldY: 0
};
var PLAYER = {};
var OPPONENT = {};
var ARMY = [];
var GAMESTATE = null;
var SAVEDGAMESTATE = null;
var SAVEDPLAYER = null;
var SAVEDOPPONENT = null;
var oldMousePos, newMousePos;
var placeWarriors = false;
var movingCamera = false;
var movingWarrior = false;
var rotatingWarrior = false;
var warriorToMove = null;
var selectedWarrior = null;
var message = "";
var question = "";
var GAMEWITHFREESPINS = false;
var GAMEWITHFBREAKFREE = false;
var freeRotationsFor = [];
var freeRotation = false;
var warriorToFace = null;
var warriorAtPos = null;
var lastWarriorAtPos = null;
var giveMovementAction = false;
var giveAttackAction = false;
var movableWarriors = [];
var fightableWarriors = [];



//##############
//### helper ###
//##############

function setRandomSeed(seed){
	Math.seedrandom(seed);
}

function inchToPixels(inch){
	return inch*25.4*2;
}

function emptyArray(array){
	if(array!=null){
		array.splice(0, array.length);
	} else {
		array = [];
	}
}



//#############
//### MOUSE ###
//#############

function mouseMoveTooltip(mouse){
	warriorAtPos = getWarriorAtPos(mouse, "position");
	//selectedWarrior = warriorAtPos;
	if(warriorAtPos!=null){
		showWarriorTooltip(warriorAtPos, mouse);
	} else {
		hideWarriorTooltip();
	}
	lastWarriorAtPos = warriorAtPos;
}

function mouseDown(mouse){
	if(mouse.which==1){
		if(!rotatingWarrior){
			oldMousePos = { x: mouse.pageX, y: mouse.pageY };
			unselectWarrior();
			var warriorToCheck = getWarriorAtPos(mouse, "position");
			selectWarrior(warriorToCheck);
			if(warriorToCheck!=null){
				// place army at the beginning
				var case1 = placeWarriors;
				// normal movement while your turn or after breakFree succeed
				var case2 = isActionPossible(warriorToCheck) && !freeRotation && (!contactWithOpponent(warriorToCheck) || (warriorToCheck.breakFreeFailed!=null && !warriorToCheck.breakFreeFailed));
				// spin after breakFree failed
				var case3 = !freeRotation && warriorToCheck.breakFreeFailed==true;
				// you have a free spin after basecontact
				var case4 = opponentsTurn() && freeRotation && isInList(warriorToCheck, freeRotationsFor);
				if(yourWarrior(warriorToCheck) && (case1 || case2 || case3 || case4)){
					warriorToMove = warriorToCheck;
					movingWarrior = true;
					if(freeRotation || warriorToMove.breakFreeFailed){
						rotatingWarrior = true;
						$("#gameCanvas").bind("mousemove", rotateWarrior);
					} else {
						$("#gameCanvas").bind("mousemove", moveWarrior);
					}
				}
			} else {
				movingCamera = true;
				$("#gameCanvas").bind("mousemove", moveCamera);
			}
		}
	}
}

function mouseUp(mouse){
	if(mouse.which==1){
		if(rotatingWarrior){
			if(placeWarriors){
				question = "Place "+warriorToMove.name+" at this point?"
				confirm(question,
					function(){
						moveTo(warriorToMove, warriorToMove.drawPosition, warriorToMove.newBaseContacts);
					},
					function(){
						revertMove();
					}
				);
			} else {
				if((freeRotation&&opponentsTurn()) || (warriorToMove.breakFreeFailed&&yourTurn())){
					var inSight = false;
					if(freeRotation&&opponentsTurn()){
						inSight = checkInSight(warriorToMove, warriorToFace, "drawPosition");
					}
					if(inSight || warriorToMove.breakFreeFailed){
						question = "Spin "+warriorToMove.name+" to this point?";
						confirm(question,
							function(){
								socket.emit("callMoveTo", compressWarrior(warriorToMove), warriorToMove.drawPosition, warriorToMove.baseContacts);
							},
							function(){
								revertMove();
							}
						);
					} else {
						newMessage(warriorToMove.name+"'s frontarc isn't in contact with "+warriorToFace.name+"'s base.");
						revertMove();
					}
				} else {
					if(warriorToMove.actions==0){
						question = "Move "+warriorToMove.name+" to this point?"
					} else {
						question = "Move "+warriorToMove.name+" to this point and inflict pushing damage?"
					}
					confirm(question,
						function(){
							socket.emit("callMoveTo", compressWarrior(warriorToMove), warriorToMove.drawPosition, warriorToMove.newBaseContacts);
						},
						function(){
							revertMove();
						}
					);
				}
			}
		} else {
			if(movingWarrior){
				$("#gameCanvas").unbind("mousemove", moveWarrior);
				if(warriorToMove.position.x!=warriorToMove.drawPosition.x || warriorToMove.position.y!=warriorToMove.drawPosition.y){
					rotatingWarrior = true;
					$("#gameCanvas").bind("mousemove", rotateWarrior);
				} else {
					movingWarrior = false;
					warriorToMove = null;
				}
			}
			if(movingCamera){
				$("#gameCanvas").unbind("mousemove", moveCamera);
				movingCamera = false;
			}
		}
	}
}



//##############
//### CAMERA ###
//##############

function mouseDownCamera(e){
	oldMousePos = { x: e.pageX, y: e.pageY };
	movingCamera = true;
	$("#gameCanvas").bind("mousemove", moveCamera);
}

function mouseUpCamera(e){
	$("#gameCanvas").unbind("mousemove", moveCamera);
	movingCamera = false;
}

function moveCamera(e) {
	if(e.buttons == 1){
		newMousePos = { x: e.pageX, y: e.pageY };
		CAMERA.x -= newMousePos.x-oldMousePos.x;
		CAMERA.y -= newMousePos.y-oldMousePos.y;
		oldMousePos = newMousePos;
		clampCamera();
	}
}

function clampCamera(){
	if(CAMERA.x < 0 - window.innerWidth/2){
		CAMERA.x = 0 - window.innerWidth/2;
	}
	if(CAMERA.x + gcc.canvas.width > GAME.TABLESIZE + window.innerWidth/2){
		CAMERA.x = GAME.TABLESIZE - gcc.canvas.width + window.innerWidth/2;
	}
	if(CAMERA.y < 0 - window.innerHeight/2){
		CAMERA.y = 0 - window.innerHeight/2;
	}
	if(CAMERA.y + gcc.canvas.height > GAME.TABLESIZE + window.innerHeight/2){
		CAMERA.y = GAME.TABLESIZE - gcc.canvas.height + window.innerHeight/2;
	}
}

function moveCameraToInitialPosition(){
	CAMERA.x = (GAME.TABLESIZE/2)-(window.innerWidth/2);
	if(PLAYER.id == GAMESTATE.p1){
		CAMERA.y = 0;
	} else {
		CAMERA.y = GAME.TABLESIZE-window.innerHeight;
	}
}



//###############
//### WARRIOR ###
//###############

function getWarrior(owner, index){
	return GAMESTATE["armies"][owner][index];
}

function compressWarrior(warrior){
	var compressedWarrior = {
		owner: warrior.owner,
		index: warrior.index
	};
	return compressedWarrior;
}

function yourWarrior(warrior){
	if(warrior.owner == PLAYER.id){
		return true;
	} else {
		return false;
	}
}

function opponentsWarrior(warrior){
	if(warrior.owner == OPPONENT.id){
		return true;
	} else {
		return false;
	}
}

function notTheSame(warriorA, warriorB){
	if(warriorA.owner!=warriorB.owner || warriorA.index!=warriorB.index){
		return true;
	} else {
		return false;
	}
}

function sameWarrior(warriorA, warriorB){
	if(warriorA.owner==warriorB.owner && warriorA.index===warriorB.index){
		return true;
	} else {
		return false;
	}
}

function isInSpawnZone(warrior){
	var p1 = warrior.owner===GAMESTATE.p1;
	var minX = 0+inchToPixels(8);
	var maxX = GAME.TABLESIZE-inchToPixels(8);
	var minY = (p1) ? 0+warrior.baseradius : GAME.TABLESIZE-inchToPixels(3);
	var maxY = (p1) ? 0+inchToPixels(3) : GAME.TABLESIZE-warrior.baseradius;
	var xIsFine = (minX<=warrior.position.x) && (warrior.position.x<=maxX);
	var yIsFine = (minY<=warrior.position.y) && (warrior.position.y<=maxY);
	//console.log(minX, warrior.position.x, maxX);
	//console.log(minY, warrior.position.y, maxY);
	return xIsFine&&yIsFine;
}

function wouldBeInSpawnZone(warrior, pos){
	var p1 = warrior.owner===GAMESTATE.p1;
	var minX = 0+inchToPixels(8);
	var maxX = GAME.TABLESIZE-inchToPixels(8);
	var minY = (p1) ? 0+warrior.baseradius : GAME.TABLESIZE-inchToPixels(3);
	var maxY = (p1) ? 0+inchToPixels(3) : GAME.TABLESIZE-warrior.baseradius;
	var xIsFine = (minX<=pos.x) && (pos.x<=maxX);
	var yIsFine = (minY<=pos.y) && (pos.y<=maxY);
	// console.log(minX, pos.x, maxX);
	// console.log(minY, pos.y, maxY);
	return xIsFine&&yIsFine;
}

function intersectsWithWarriors(warrior){
	var intersects = false;
	for(var army in GAMESTATE["armies"]){
		for(var target in GAMESTATE["armies"][army]){
			target = getWarrior(army, target);
			if(notTheSame(warrior, target) && getDistance(warrior, target)<warrior.baseradius+target.baseradius){
				intersects = true;
				break;
			}
		}
		if(intersects) break;
	}
	return intersects;
}

function wouldIntersectWithWarriors(warrior, pos){
	var wouldIntersect = false;
	for(var army in GAMESTATE["armies"]){
		for(var target in GAMESTATE["armies"][army]){
			target = getWarrior(army, target);
			if(notTheSame(warrior, target) && distance(pos, target.position)<warrior.baseradius+target.baseradius){
				wouldIntersect = true;
				break;
			}
		}
		if(wouldIntersect) break;
	}
	return wouldIntersect;
}

function contactWithOpponent(warrior){
	for(var i=0; i<warrior.baseContacts.length; i++){
		if(warrior.baseContacts[i].owner==OPPONENT.id){
			return true;
		}
	}
	return false;
}

function isDemoralized(warrior){
	if(warrior.activeState.damageboni.indexOf("[yellow]")===-1){
		return false;
	} else {
		return true;
	}
}

function isMovable(warrior){
	if(warrior.gotAction || warrior.actions>=2 || (isDemoralized(warrior) && warrior.actions===1)){
		return false;
	} else {
		return true;
	}
}

function canMelee(warrior){
	if(getMeleeTargets(warrior).length>0){
		return true;
	} else {
		return false;
	}
}

function canRange(warrior){
	if(!contactWithOpponent(warrior) && getRangeTargets(warrior).length>0){
		return true;
	} else {
		return false;
	}
}

function getMeleeTargets(warrior){
	var targets = [];
	if(contactWithOpponent(warrior)){
		for(var i=0; i<warrior.baseContacts.length; i++){
			var contact = warrior.baseContacts[i];
			contact = getWarrior(contact.owner, contact.index);
			if(!yourWarrior(contact) && checkInSight(warrior, contact, "position")){
				targets.push(contact);
			}
		}
	}
	return targets;
}

function getRangeTargets(warrior){
	var targets = [];
	getWarriorsInRange(warrior);
	for(var i=0; i<warrior.rangeTargets.length; i++){
		var target = getWarrior(warrior.rangeTargets[i].owner, warrior.rangeTargets[i].index);
		if(opponentsWarrior(target)) targets.push(target);
	}
	return targets;
}

function getWeakestTarget(warrior, targets){
	var lowestDef = Number.MAX_VALUE;
	var weakestTarget = null;
	for(var i=0; i<targets.length; i++){
		var target = targets[i];
		var def = target.activeState.defence;
		if(def<lowestDef){
			lowestDef = def;
			weakestTarget = target;
		}
	}
	return weakestTarget;
}

function getStrongestTarget(warrior, targets){
	var highestAtk = 0;
	var strongestTarget = null;
	for(var i=0; i<targets.length; i++){
		var target = targets[i];
		var atk = target.activeState.attack+target.attackbonus;
		if(atk>highestAtk){
			highestAtk = atk;
			strongestTarget = target;
		}
	}
	return strongestTarget;
}

function getKiteTarget(warrior){
	var possibleTargets = [];
	var kiteTarget = null;
	for(var enemy in GAMESTATE["armies"][OPPONENT.id]){
		enemy = GAMESTATE["armies"][OPPONENT.id][enemy];
		var cantMelee = enemy.activeState.movementrange+enemy.baseradius+warrior.baseradius<inchToPixels(warrior.range);
		var cantRange = enemy.range<inchToPixels(warrior.range);
		if(cantMelee && cantRange) possibleTargets.push(enemy);
	}
	if(possibleTargets.length>0){
		kiteTarget = getWeakestTarget(warrior, possibleTargets);
		return kiteTarget;
	} else {
		return null;
	}
}

function selectWarrior(warrior){
	selectedWarrior = warrior;
	updateUI();
}

function unselectWarrior(){
	selectedWarrior = null;
	updateUI();
}

function rotateWarrior(mouse){
	var v1 = {x: (mouse.pageX+CAMERA.x)-warriorToMove.drawPosition.x, y: (mouse.pageY+CAMERA.y)-warriorToMove.drawPosition.y};
	var v2 = {x: 1, y: 0};
	var angle = Math.acos((v1.x*v2.x+v1.y*v2.y)/(Math.hypot(v1.x, v1.y)*Math.hypot(v2.x, v2.y)))*180/Math.PI;
	if(mouse.pageY+CAMERA.y<=warriorToMove.drawPosition.y){
		angle = 360-angle;
	}
	warriorToMove.drawPosition.rotation = angle;
	angle = warriorToMove.drawPosition.rotation*Math.PI/180;
	warriorToMove.drawPosition.direction.x = 1*Math.cos(angle)-0*Math.sin(angle);
	warriorToMove.drawPosition.direction.y = 1*Math.sin(angle)+0*Math.cos(angle);
}

function moveWarrior(mouse){
	var collidedWarriors = [];
	for(var army in GAMESTATE["armies"]){
		for(var warrior in GAMESTATE["armies"][army]){
			var distMouse = Math.hypot((mouse.pageX+CAMERA.x)-GAMESTATE["armies"][army][warrior].position.x, (mouse.pageY+CAMERA.y)-GAMESTATE["armies"][army][warrior].position.y)
			var distOpponent = Math.hypot(warriorToMove.drawPosition.x-GAMESTATE["armies"][army][warrior].position.x, warriorToMove.drawPosition.y-GAMESTATE["armies"][army][warrior].position.y)
			var range = GAMESTATE["armies"][army][warrior].baseradius+warriorToMove.baseradius;
			if((distMouse<range || distOpponent<range) && GAMESTATE["armies"][army][warrior]!=warriorToMove){
				collidedWarriors.push(GAMESTATE["armies"][army][warrior]);
			}
		}
	}
	if(collidedWarriors.length==1){
		var distMouse = Math.hypot((mouse.pageX+CAMERA.x)-collidedWarriors[0].position.x, (mouse.pageY+CAMERA.y)-collidedWarriors[0].position.y);
		if(distMouse<=collidedWarriors[0].baseradius+warriorToMove.baseradius || PLAYER.isAI){
			var newX = collidedWarriors[0].position.x+((((mouse.pageX+CAMERA.x)-collidedWarriors[0].position.x)/distMouse)*(collidedWarriors[0].baseradius+warriorToMove.baseradius));
			var newY = collidedWarriors[0].position.y+((((mouse.pageY+CAMERA.y)-collidedWarriors[0].position.y)/distMouse)*(collidedWarriors[0].baseradius+warriorToMove.baseradius));
			var range = Math.hypot(newX-warriorToMove.position.x, newY-warriorToMove.position.y);
			if(range<inchToPixels(warriorToMove.activeState.movementrange) || placeWarriors || PLAYER.isAI){
				warriorToMove.drawPosition.x = newX;
				warriorToMove.drawPosition.y = newY;
			} else {
				//case a: the calculated position will be out of the movementrange.
				console.log("a: ");
			}
		} else {
			//case b:
			console.log("b: ");
		}
		warriorToMove.newBaseContacts = [];
		warriorToMove.newBaseContacts.push(compressWarrior(collidedWarriors[0]));
	} else if(collidedWarriors.length>1){
		var det = (collidedWarriors[1].position.x-collidedWarriors[0].position.x)*(mouse.pageY+CAMERA.y-collidedWarriors[0].position.y) - (collidedWarriors[1].position.y-collidedWarriors[0].position.y)*(mouse.pageX+CAMERA.x-collidedWarriors[0].position.y);
		if(det>0){
			var A = collidedWarriors[1];
			var B = collidedWarriors[0];
		} else if(det<=0){
			var A = collidedWarriors[0];
			var B = collidedWarriors[1];
		}
		var C = warriorToMove;
		var a = B.baseradius+C.baseradius;
		var b = A.baseradius+C.baseradius;
		var c = Math.hypot(B.position.x-A.position.x, B.position.y-A.position.y);
		var rotation = -Math.acos((b*b+c*c-a*a)/(2*b*c));
		var x = B.position.x-A.position.x;
		var y = B.position.y-A.position.y;
		var x1 = x*Math.cos(rotation)-y*Math.sin(rotation);
		var y1 = x*Math.sin(rotation)+y*Math.cos(rotation);
		var dist = Math.hypot(x1,y1);
		x1 = (x1/dist)*(collidedWarriors[0].baseradius+warriorToMove.baseradius);
		y1 = (y1/dist)*(collidedWarriors[0].baseradius+warriorToMove.baseradius);
		warriorToMove.drawPosition.x = A.position.x+x1;
		warriorToMove.drawPosition.y = A.position.y+y1;
		warriorToMove.newBaseContacts = [];
		warriorToMove.newBaseContacts.push(compressWarrior(collidedWarriors[0]));
		warriorToMove.newBaseContacts.push(compressWarrior(collidedWarriors[1]));
	} else {
		var range = Math.hypot((mouse.pageX+CAMERA.x)-warriorToMove.position.x, (mouse.pageY+CAMERA.y)-warriorToMove.position.y);
		if(range<inchToPixels(warriorToMove.activeState.movementrange) || placeWarriors){
			warriorToMove.drawPosition.x = mouse.pageX+CAMERA.x;
			warriorToMove.drawPosition.y = mouse.pageY+CAMERA.y;
		} else {
			warriorToMove.drawPosition.x = (((mouse.pageX+CAMERA.x)-warriorToMove.position.x)/range)*inchToPixels(warriorToMove.activeState.movementrange)+warriorToMove.position.x;
			warriorToMove.drawPosition.y = (((mouse.pageY+CAMERA.y)-warriorToMove.position.y)/range)*inchToPixels(warriorToMove.activeState.movementrange)+warriorToMove.position.y;
		}
		warriorToMove.newBaseContacts = [];
	}
	clampWarrior(warriorToMove);
}

function clampWarrior(warrior){
	if(placeWarriors && !DEBUG){
		var borderLeft = 0+inchToPixels(8);
		var borderRight = GAME.TABLESIZE-inchToPixels(8);
		if(warrior.owner == GAMESTATE.p1){
			var borderTop = 0+warrior.baseradius;
			var borderBottom = 0+inchToPixels(3);
		} else {
			var borderTop = GAME.TABLESIZE-inchToPixels(3);
			var borderBottom = GAME.TABLESIZE-warrior.baseradius;
		}

		if(warrior.drawPosition.x < borderLeft){
			warrior.drawPosition.x = borderLeft;
		}
		if(warrior.drawPosition.x > borderRight){
			warrior.drawPosition.x = borderRight;
		}
		if(warrior.drawPosition.y < borderTop){
			warrior.drawPosition.y = borderTop;
		}
		if(warrior.drawPosition.y > borderBottom){
			warrior.drawPosition.y = borderBottom;
		}
	} else {
		if(warrior.drawPosition.x - warrior.baseradius < 0){
			warrior.drawPosition.x = warrior.baseradius;
		}
		if(warrior.drawPosition.x + warrior.baseradius > GAME.TABLESIZE){
			warrior.drawPosition.x = GAME.TABLESIZE - warrior.baseradius;
		}
		if(warrior.drawPosition.y-warrior.baseradius < 0){
			warrior.drawPosition.y = warrior.baseradius;
		}
		if(warrior.drawPosition.y + warrior.baseradius > GAME.TABLESIZE){
			warrior.drawPosition.y = GAME.TABLESIZE - warrior.baseradius;
		}
	}
}

function moveTo(warrior, newPosition, newBaseContacts){
	if(freeRotation){
		removeFromList(warrior, freeRotationsFor);
	} else {
		emptyArray(freeRotationsFor);
		for(var i=0; i<warrior.baseContacts.length; i++){
			var owner = warrior.baseContacts[i].owner;
			var index = warrior.baseContacts[i].index;
			var contact = getWarrior(owner, index);
			var oldContacts = contact.baseContacts;
			var newContacts = [];
			for(var j=0; j<oldContacts.length; j++){
				if(oldContacts[j].owner!=warrior.owner || oldContacts[j].index!=warrior.index){
					newContacts.push(oldContacts[j]);
				}
			}
			emptyArray(contact.baseContacts);
			for(var j=0; j<newContacts.length; j++){
				contact.baseContacts.push(newContacts[j]);
			}
		}
		for(var i=0; i<newBaseContacts.length; i++){
			var contact = getWarrior(newBaseContacts[i].owner, newBaseContacts[i].index);
			contact.baseContacts.push(compressWarrior(warrior));
			if(GAMEWITHFREESPINS && !isInList(contact, warrior.baseContacts) && contact.owner==GAMESTATE.inactivePlayer && !placeWarriors){
				freeRotationsFor.push(newBaseContacts[i]);
				warriorToFace = warrior;
			}
		}
	}
	warrior.oldPosition = (placeWarriors) ? copyPosition(newPosition) : copyPosition(warrior.position);
	warrior.position = copyPosition(newPosition);
	warrior.drawPosition = copyPosition(newPosition);
	warrior.baseContacts = newBaseContacts;
	warriorToMove = null;
	movingWarrior = false;
	rotatingWarrior = false;
	$("#gameCanvas").unbind("mousemove", rotateWarrior);
	if(!placeWarriors && !freeRotation && !warrior.breakFreeFailed){
		newMessage(MESSAGE.MOVED(warrior));
		giveActionToken(warrior);
	} else if(freeRotation || warrior.breakFreeFailed){
		newMessage(MESSAGE.SPINNED(warrior));
		warrior.breakFreeFailed = null;
	}
	if(freeRotationsFor.length>0){
		freeRotation = true;
		newMessage(MESSAGE.FREESPINS(yourTurn()));
	} else if(freeRotation){
		freeRotation = false;
		warriorToFace = null;
		newMessage(MESSAGE.SPINSAPPLIED);
	}
	updateAllRangeTargets();
	updateUI();
}

function revertMove(){
	if(warriorToMove!=null){
		warriorToMove.drawPosition = copyPosition(warriorToMove.position);
		warriorToMove.newBaseContacts = warriorToMove.baseContacts;
		warriorToMove = null;
		movingWarrior = false;
		$("#gameCanvas").unbind("mousemove", moveWarrior);
		rotatingWarrior = false;
		$("#gameCanvas").unbind("mousemove", rotateWarrior);
	}
}

function breakFree(warrior){
	var diceValue = rollDice(1);
	newMessage(MESSAGE.DICEROLL(diceValue, yourTurn()));
	if(diceValue>3){
		newMessage(MESSAGE.BREAKFREE(warrior));
		warrior.breakFreeFailed = false;
	} else {
		newMessage(MESSAGE.BREAKFREEFAILED(warrior));
		warrior.breakFreeFailed = true;
		giveActionToken(warrior);
	}
}

function abortSpins(){
	emptyArray(freeRotationsFor);
	freeRotation = false;
	warriorToFace = null;
	updateUI();
	newMessage(MESSAGE.SPINSCANCLED);
}

function getWarriorAtPos(mouse, pos){
	for(var army in GAMESTATE["armies"]){
		for(var warrior in GAMESTATE["armies"][army]){
			warrior = GAMESTATE["armies"][army][warrior];
			if(checkPosForWarrior(mouse, warrior, pos)){
				return warrior;
			}
		}
	}
	return null;
}


function checkPosForWarrior(mouse, warrior, pos){
	var dist = Math.hypot(warrior[pos].x-(mouse.pageX+CAMERA.x), warrior[pos].y-(mouse.pageY+CAMERA.y));
	if(dist <= warrior.baseradius){
		return true;
	} else {
		return false;
	}
}

function copyPosition(pos){
	var posCopy = {
		x: pos.x,
		y: pos.y,
		rotation: pos.rotation,
		direction: {
			x: pos.direction.x,
			y: pos.direction.y
		}
	}
	return posCopy;
}

function isInList(warrior, list){
	for(var i=0; i<list.length; i++){
		if(warrior.owner==list[i].owner && warrior.index==list[i].index){
			return true;
		}
	}
	return false;
}

function removeFromList(warrior, list){
	var index;
	for(var i=0; i<list.length; i++){
		if(warrior.owner==list[i].owner && warrior.index==list[i].index){
			index = i;
		}
	}
	list.splice(index, 1);
}



//############
//### ARMY ###
//############

function addArmy(army){
	for(var i=0; i<army.length; i++){
		addToArmy(army[i]);
	}
}

function addToArmy(id){
	if(getArmySize()+warriorDB[id].costs <= GAME.MAXARMYSIZE){
		if(warriorDB[id].rank!="Unique"){
			ARMY.push(id);
			ARMY.sort();
		} else {
			if(ARMY.indexOf(id)==-1){
				ARMY.push(id);
				ARMY.sort();
			} else {
				newMessage(MESSAGE.WARRIORUNIQUE);
			}
		}
	} else {
		newMessage(MESSAGE.ARMYTOBIG);
	}
	updateArmyList();
}

function removeFromArmy(index){
	ARMY.splice(index, 1);
	updateArmyList();
}

function getArmySize(){
	var size = 0;
	for(var i=0; i<ARMY.length; i++){
		size+=warriorDB[ARMY[i]].costs;
	}
	return size;
}

function getRemainingArmySize(player){
	var armysize = 0;
	for(var warrior in GAMESTATE["armies"][player]){
		armysize+=GAMESTATE["armies"][player][warrior].costs;
	}
	return armysize;
}

function getRemainingWarriors(player){
	return Object.keys(GAMESTATE["armies"][player]).length;
}

function moveWarriorsToInitialPosition(){
	for(var army in GAMESTATE["armies"]){
		var dist = 75;
		var i = 0;
		var line = 5;
		for(var warrior in GAMESTATE["armies"][army]){
			warrior = GAMESTATE["armies"][army][warrior];
			if(i%2==0){
				warrior.position.x = GAME.TABLESIZE/2 - (i+1)*dist;
			} else {
				warrior.position.x = GAME.TABLESIZE/2 + (i)*dist;
			}
			if(warrior.owner == GAMESTATE.p1){
				warrior.position.y = 0+inchToPixels(line);
				//TODO rotate warrior to the bottom (rotation and direction)
			} else {
				warrior.position.y = GAME.TABLESIZE-inchToPixels(line);
				//TODO rotate warrior to the top (rotation and direction)
			}
			warrior.oldPosition = copyPosition(warrior.position);
			warrior.drawPosition = copyPosition(warrior.position);
			if((i+1)*dist <= (GAME.TABLESIZE-inchToPixels(16))/2){
				i++;
			} else {
				i=0;
				line += 2;
			}
		}
	}
}

function validateSpawnPositions(owner){
	for(var warrior in GAMESTATE["armies"][owner]){
		warrior = GAMESTATE["armies"][owner][warrior];
		var x = warrior.position.x;
		var y = warrior.position.y;

		var borderLeft = 0+inchToPixels(8);
		var borderRight = GAME.TABLESIZE-inchToPixels(8);
		if(warrior.owner == GAMESTATE.p1){
			var borderTop = 0+warrior.baseradius;
			var borderBottom = 0+inchToPixels(3);
		} else {
			var borderTop = GAME.TABLESIZE-inchToPixels(3);
			var borderBottom = GAME.TABLESIZE-warrior.baseradius;
		}

		if(x<borderLeft || x>borderRight || y<borderTop || y>borderBottom){
			return false;
		}
	}
	return true;
}

function getArmyPositions(owner){
	var positions = [];
	for(var index in GAMESTATE["armies"][owner]){
		var warrior = getWarrior(owner, index);
		var pos = {
			owner: warrior.owner,
			index: warrior.index,
			position: warrior.position,
			drawPosition: warrior.drawPosition,
			baseContacts: warrior.baseContacts
		}
		positions.push(pos);
	}
	return positions;
}

function setArmyPositions(positions){
	for(var i=0; i<positions.length; i++){
		GAMESTATE["armies"][positions[i].owner][positions[i].index].position = positions[i].position;
		GAMESTATE["armies"][positions[i].owner][positions[i].index].drawPosition = positions[i].drawPosition;
		GAMESTATE["armies"][positions[i].owner][positions[i].index].oldPosition = positions[i].position;
		GAMESTATE["armies"][positions[i].owner][positions[i].index].baseContacts = positions[i].baseContacts;
	}
}

function getMovableWarriors(){
	emptyArray(movableWarriors);
	for(var warrior in GAMESTATE["armies"][GAMESTATE.activePlayer]){
		warrior = GAMESTATE["armies"][GAMESTATE.activePlayer][warrior];
		if(isActionPossible(warrior)){
			movableWarriors.push(warrior);
		}
	}
}

function getFightableWarriors(){
	emptyArray(fightableWarriors);
	for(var warrior in GAMESTATE["armies"][GAMESTATE.activePlayer]){
		warrior = GAMESTATE["armies"][GAMESTATE.activePlayer][warrior];
		if(isActionPossible(warrior)){
			fightableWarriors.push(warrior);
		}
	}
}



//############
//### GAME ###
//############

function rollDice(rolls){
	var sides = 6;
	var result = 0;
	for(var i=0; i<rolls; i++){
		result += (!SIMULATING) ? Math.floor(Math.random()*sides)+1 : Math.floor(random()*sides)+1;
	}
	return result;
}

function giveActionToken(warrior){
	warrior.gotAction = true;
	warrior.actions++;
	if(warrior.actions==2){
		newMessage(MESSAGE.PUSHINGDMG(warrior));
		inflictDamage(warrior, 1)
	}
	GAMESTATE.executedActions++;
	updateUI();
}

function removeActionToken(warrior){
	if(!warrior.gotAction && warrior.actions>0){
		warrior.actions--;
	}
	warrior.gotAction = false;
}

//fightClose
function fightClose(attacker, defender){
	var diceValue = rollDice(2);
	var attackbonus = (attacker.attacktype==="sowrd") ? attacker.attackbonus : 0;
	var attackValue = diceValue + attacker.activeState.attack + attackbonus;
	var dmgValue = attacker.activeState.damage;
	var defenceValue = defender.activeState.defence;
	var criticalMiss = false;
	var criticalHit = false;
	var rearAttack = checkRearArcAttack(attacker, defender, "position")

	if(diceValue==2){
		criticalMiss = true;
	} else if(diceValue==12){
		criticalHit = true;
	}

	if(!criticalMiss){
		if(rearAttack){
			attackValue++;
			newMessage(MESSAGE.REARATTACK);
		}
		newMessage(MESSAGE.DICEROLL(diceValue, yourTurn()));
		newMessage(MESSAGE.ATTACK(attacker, defender, attackValue, defenceValue));
		if(attackValue >= defenceValue || criticalHit){
			if(criticalHit){
				newMessage(MESSAGE.CRITICALHIT);
				newMessage(MESSAGE.DMG(defender, dmgValue+1));
				inflictDamage(defender, dmgValue+1);
			} else {
				newMessage(MESSAGE.HIT);
				newMessage(MESSAGE.DMG(defender, dmgValue));
				inflictDamage(defender, dmgValue);
			}
		} else {
			newMessage(MESSAGE.NOHIT);
		}
	} else {
		newMessage(MESSAGE.CRITICALMISS(attacker));
	}

	giveActionToken(attacker);
	updateUI();
}

function fightRange(attacker, defenders){
	var diceValue = rollDice(2);
	var attackbonus = (attacker.attacktype!="sowrd") ? attacker.attackbonus : 0;
	var baseAttackValue = diceValue + attacker.activeState.attack + attackbonus;
	var dmgValue = attacker.rangedamage;
	var defenceValue = 0;
	var hittedTargets = [];
	var criticalMiss = false;
	var criticalHit = false;

	if(diceValue==2){
		criticalMiss = true;
	} else if(diceValue==12){
		criticalHit = true;
	}

	if(!criticalMiss){
		for(var i=0; i<defenders.length; i++){
			var defender = getWarrior(defenders[i].owner, defenders[i].index);
			if(attacker.attacktype!="wand" || defender.defencetype!="magic"){
				var attackValue = baseAttackValue;
				if(checkRearArcAttack(attacker, defender, "position")){
					attackValue++;
					newMessage(MESSAGE.REARATTACK);
				}
				defenceValue = defender.activeState.defence;
				newMessage(MESSAGE.DICEROLL(diceValue, yourTurn()));
				newMessage(MESSAGE.ATTACK(attacker, defender, attackValue, defenceValue));
				if(attackValue >= defenceValue || criticalHit){
					if(criticalHit){
						newMessage(MESSAGE.CRITICALHIT);
					} else {
						newMessage(MESSAGE.HIT);
					}
					hittedTargets.push(i);
				} else {
					newMessage(MESSAGE.NOHIT);
				}
			} else {
				newMessage("Target is magic imune.");
			}
		}
	} else {
		newMessage(MESSAGE.CRITICALMISS(attacker));
	}

	for(var i=0; i<hittedTargets.length; i++){
		var defender = getWarrior(defenders[hittedTargets[i]].owner, defenders[hittedTargets[i]].index);
		if(dmgValue>0){
			//TODO damageselect for every hitted target
			if(criticalHit){
				newMessage(MESSAGE.DMG(defender, dmgValue+1));
				inflictDamage(defender, dmgValue+1);
			} else {
				newMessage(MESSAGE.DMG(defender, dmgValue));
				inflictDamage(defender, dmgValue);
			}
			dmgValue--;
		}
	}

	giveActionToken(attacker);
	updateUI();
}

function inflictDamage(warrior, dmg){
	warrior.damageTaken+=dmg;
	if(warrior.damageTaken >= warrior.life){
		newMessage(MESSAGE.DEAD(warrior));
		if(warrior.baseContacts.length!=0){
			for(var i=0; i<warrior.baseContacts.length; i++){
				var contact = getWarrior(warrior.baseContacts[i].owner, warrior.baseContacts[i].index);
				removeFromList(warrior, contact.baseContacts);
			}
		}
		delete GAMESTATE["armies"][warrior.owner][warrior.index];
	} else {
		newMessage(MESSAGE.NOTDEAD(warrior));
		warrior.activeState = warrior.state[warrior.damageTaken];
	}
	checkDefeatedArmy();
}

function checkDefeatedArmy(){
	var p1 = GAMESTATE.p1;
	var p2 = GAMESTATE.p2;
	if((checkEmptyArmy(p1) || checkDemoralizedArmy(p1)) && (checkEmptyArmy(p2) || checkDemoralizedArmy(p2))){
		//draw
		endGame("draw", "Both armies are dead.");
	} else {
		if(checkEmptyArmy(p1) || checkDemoralizedArmy(p1)){
			//p1 dead, p2 wins
			endGame(p2, "Army was defeated.");
		}
		if(checkEmptyArmy(p2) || checkDemoralizedArmy(p2)){
			//p2 dead, p1 wins
			endGame(p1, "Army was defeated.");
		}
	}
}

function checkEmptyArmy(player){
	return jQuery.isEmptyObject(GAMESTATE["armies"][player]);
}

function checkDemoralizedArmy(player){
	var demoralized = true;
	for(var warrior in GAMESTATE["armies"][player]){
		warrior = getWarrior(player, warrior);
		demoralized = isDemoralized(warrior);
		if(!demoralized) break;
	}
	return demoralized;
}

function checkInSight(warriorA, warriorB, pos){
	if(pos=="position"){
		var v1 = {x: warriorA.position.direction.x, y: warriorA.position.direction.y};
		var v2 = {x: warriorB.position.x-warriorA.position.x, y: warriorB.position.y-warriorA.position.y};
	} else if(pos=="drawPosition"){
		var v1 = {x: warriorA.drawPosition.direction.x, y: warriorA.drawPosition.direction.y};
		var v2 = {x: warriorB.position.x-warriorA.drawPosition.x, y: warriorB.position.y-warriorA.drawPosition.y};
	}
	var angle = Math.acos((v1.x*v2.x+v1.y*v2.y)/(Math.hypot(v1.x, v1.y)*Math.hypot(v2.x, v2.y)))*180/Math.PI;
	if(angle<=warriorA.frontarc*15){
		return true;
	}
	return false;
}

function checkRearArcAttack(warriorA, warriorB, pos){
	var rearArcAttack = false;
	if(pos=="position"){
		var v1 = {x: 0-warriorB.position.direction.x, y: 0-warriorB.position.direction.y};
		var v2 = {x: warriorA.position.x-warriorB.position.x, y: warriorA.position.y-warriorB.position.y};
	} else if(pos=="drawPosition"){
		var v1 = {x: 0-warriorB.position.direction.x, y: 0-warriorB.position.direction.y};
		var v2 = {x: warriorA.drawPosition.x-warriorB.position.x, y: warriorA.drawPosition.y-warriorB.position.y};
	}
	var angle = Math.acos((v1.x*v2.x+v1.y*v2.y)/(Math.hypot(v1.x, v1.y)*Math.hypot(v2.x, v2.y)))*180/Math.PI;
	if(angle<=warriorA.reararc*15){
		rearArcAttack = true;
	}
	return rearArcAttack;
}

function checkInRange(warriorA, warriorB){
	if(getDistance(warriorA, warriorB)<=inchToPixels(warriorA.range)){
		return true;
	} else {
		return false;
	}
}

function checkFreeSight(warriorA, warriorB){
	for(var army in GAMESTATE["armies"]){
		for(var warrior in GAMESTATE["armies"][army]){
			var warriorC = GAMESTATE["armies"][army][warrior];
			var distAB = getDistance(warriorA, warriorB);
			var distAC = getDistance(warriorA, warriorC);
			if(!sameWarrior(warriorC, warriorA) && !sameWarrior(warriorC, warriorB) && distAC<distAB && checkInSight(warriorA, warriorC, "position")){
				var x0 = warriorC.position.x;
				var y0 = warriorC.position.y;
				var x1 = warriorA.position.x;
				var y1 = warriorA.position.y;
				var x2 = warriorB.position.x;
				var y2 = warriorB.position.y;
				var dist = Math.abs((y2-y1)*x0-(x2-x1)*y0+(x2*y1)-(y2*x1))/getDistance(warriorA, warriorB);
				if(dist<warriorC.baseradius){
					return false;
				}
			}
		}
	}
	return true;
}

function getWarriorsInRange(warrior){
	emptyArray(warrior.rangeTargets);
	if(warrior.range>0 && warrior.baseContacts.length==0){
		for(var army in GAMESTATE["armies"]){
			for(var target in GAMESTATE["armies"][army]){
				target = GAMESTATE["armies"][army][target];
				if(!sameWarrior(target, warrior)){
					if(checkInRange(warrior, target) && checkInSight(warrior, target, "position") && checkFreeSight(warrior, target)){
						warrior.rangeTargets.push(compressWarrior(target));
					}
				}
			}
		}
	}
}

function updateAllRangeTargets(){
	for(var army in GAMESTATE["armies"]){
		for(var warrior in GAMESTATE["armies"][army]){
			getWarriorsInRange(GAMESTATE["armies"][army][warrior]);
		}
	}
}

function getDistance(warriorA, warriorB){
	return Math.hypot(warriorB.position.x-warriorA.position.x, warriorB.position.y-warriorA.position.y);
}

function checkForControlPoint(warrior){
	for(var cp in GAMESTATE.controlpoints){
		cp = GAMESTATE.controlpoints[cp];
		dist = Math.hypot(cp.position.x-warrior.position.x, cp.position.y-warrior.position.y);
		if(dist<warrior.baseradius+cp.radius){
			return true;
		}
	}
	return false;
}

function checkControlPointsOwner(){
	var owners = [];
	for(var cp in GAMESTATE["controlpoints"]){
		var CP = GAMESTATE["controlpoints"][cp];
		emptyArray(CP.baseContacts);
		for(var army in GAMESTATE["armies"]){
			for(warrior in GAMESTATE["armies"][army]){
				warrior = GAMESTATE["armies"][army][warrior];
				var dist = Math.hypot(CP.position.x-warrior.position.x, CP.position.y-warrior.position.y);
				if(dist<warrior.baseradius+CP.radius){
					CP.baseContacts.push(compressWarrior(warrior));
				}
			}
		}
		if(CP.baseContacts.length>0){
			var yours = 0;
			var others = 0;
			for(var i=0; i<CP.baseContacts.length; i++){
				if(yourCP(CP.baseContacts[i])){
					yours++;
				} else {
					others++;
				}
			}
			if(yours>others && CP.owner!=PLAYER.id){
				newMessage(MESSAGE.YOUCAPPED(CP));
				CP.owner = PLAYER.id;
			} else if(others>yours && CP.owner!=OPPONENT.id){
				newMessage(MESSAGE.OPPONENTCAPPED(CP));
				CP.owner = OPPONENT.id;
			} else if(yours==others){
				newMessage(MESSAGE.CONTESTED(CP))
				CP.owner = null;
			}
		}
		owners.push(CP.owner);
	}
	var sameOwner = true;
	var lastOwner = null;
	for(var i=0; i<owners.length; i++){
		if(i===0){
			lastOwner = owners[i];
		} else {
			if(owners[i]!=lastOwner){
				sameOwner = false;
				break;
			}
		}
	}
	if(lastOwner!=null && sameOwner){
		if(lastOwner===GAMESTATE.allCpHold.from){
			GAMESTATE.allCpHold.rounds++;
		} else {
			GAMESTATE.allCpHold.from = lastOwner;
			GAMESTATE.allCpHold.rounds = 0;
		}
	} else {
		GAMESTATE.allCpHold.from = null;
		GAMESTATE.allCpHold.rounds = null;
	}
	if(GAMESTATE.allCpHold.rounds/2>=GAME.ROUNDSTOWIN) endGame(GAMESTATE.allCpHold.from, "CPs hold for needed rounds.");
}

function yourCP(cp){
	if(cp.owner == PLAYER.id){
		return true;
	} else {
		return false;
	}
}

function opponentsCP(cp){
	if(cp.owner == OPPONENT.id){
		return true;
	} else {
		return false;
	}
}

function endPhase(){
	if(!GAMESTATE.gameover){
		checkControlPointsOwner();
		for(var warrior in GAMESTATE["armies"][GAMESTATE.activePlayer]){
			warrior = GAMESTATE["armies"][GAMESTATE.activePlayer][warrior];
			removeActionToken(warrior);
			warrior.breakFreeFailed = null;
		}
		if(!SIMULATING) console.log("executedActions", GAMESTATE.executedActions);
		GAMESTATE.executedActions = 0;
		updateUI();
	}
}

function endTurn(){
	revertMove();
	endPhase();
	if(!GAMESTATE.gameover){
		if(MAKESCREENSHOTS && !SIMULATING) downloadScreenshot("mko_screenshot_"+GAMESTATE.seed+"_afterTurn"+GAMESTATE.turn+"_("+GAMESTATE.activePlayer+")");
		//TODO create actionslog to get info what actions were executed in the screenshots
		if(GAMESTATE.activePlayer == GAMESTATE.p1){
			GAMESTATE.activePlayer = GAMESTATE.p2;
			GAMESTATE.inactivePlayer = GAMESTATE.p1;
		} else {
			GAMESTATE.activePlayer = GAMESTATE.p1;
			GAMESTATE.inactivePlayer = GAMESTATE.p2;
		}
		newMessage(MESSAGE.ENDTURN(yourTurn()));
		//if(!SIMULATING) console.log("CurrentFitness: ", calculateFitness(GAMESTATE, true));
		if(!SIMULATING) console.log("### TURN END ###");
		if(SIMULATINGGAME) swapUser();
		nextTurn();
	} else {
		if(MAKESCREENSHOTS) downloadScreenshot("mko_screenshot_"+GAMESTATE.seed+"_"+GAMESTATE.turn+"_(final)");
	}
}

function nextTurn(){
	if(SIMULATINGGAME) SIMULATINGGAME++
	if(SIMULATINGGAME>MAXTURNS) SIMULATINGGAME = 0;
	GAMESTATE.turn++;
	updateUI();
}

function endGame(winner, reason){
	if(winner=="draw"){
		GAMESTATE.winner = "draw";
		newMessage("It's a draw! "+reason);
	} else {
		if(winner==PLAYER.id){
			GAMESTATE.winner = PLAYER.id;
			newMessage("You win! "+reason);
		} else {
			GAMESTATE.winner = OPPONENT.id;
			newMessage("You lost! "+reason);
		}
	}
	if(!SIMULATING) alert("Game is over... console for more information.");
	if(!SIMULATING) console.log("print gameevaluation here...");
	actions = [];
	GAMESTATE.gameover = true;
	//TODO return to lobby or something else
}

function yourTurn(){
	if(GAMESTATE.activePlayer == PLAYER.id){
		return true;
	} else {
		return false;
	}
}

function opponentsTurn(){
	if(GAMESTATE.activePlayer == OPPONENT.id){
		return true;
	} else {
		return false;
	}
}

function isActionPossible(warrior){
	if(warrior["actions"]<2 && !warrior["gotAction"] && GAMESTATE.executedActions<=2 && yourTurn()){
		return true;
	} else {
		return false;
	}
}


//#########################################
var startTimes = {};
function startTimer(flag){
	if(flag && !startTimes.hasOwnProperty(flag)){
		startTimes[flag] = Date.now();
	}
}
function stopTimer(flag){
	if(flag in startTimes){
		var delta = (Date.now()-startTimes[flag])/1000.0;
		console.log(flag, delta);
		delete startTimes[flag];
		return delta;
	} else {
		console.log("No such flag, Date.now():", Date.now());
	}
}
function avg(array){
	var sum = 0;
	for(var i=0; i<array.length; i++){
		sum+= array[i];
	}
	return sum/array.length;
}
