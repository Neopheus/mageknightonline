var FPS = 30;
var INTERVAL = 1000/FPS;
var MAKESCREENSHOTS = false;
var SCREENSHOTING = false;

var overviewWidth = 200;

function updateCanvas(){
	if(!SCREENSHOTING){
		gcc.canvas.width = window.innerWidth;
		gcc.canvas.height = window.innerHeight;
	}
	drawTable();
	if(placeWarriors) drawSpawns();
	drawControlPoints();
	drawMovedLines();
	drawWarriors();
	drawSelectedWarrior();
	if(giveAttackAction){
		gcc.fillStyle = COLOR.RED;
		gcc.fillRect(0,0,10,10);
		//TODO implement drawFightableWarriors();
	}
	if(giveMovementAction){
		gcc.fillStyle = COLOR.GREEN;
		gcc.fillRect(10,0,10,10);
		//TODO implement drawMovableWarriors();
	}
}

function drawBackgroundImage(){
	gcc.drawImage(imgTable, CAMERA.x, CAMERA.y, gcc.canvas.width, gcc.canvas.height, 0, 0, gcc.canvas.width, gcc.canvas.height);
}

function drawTable(){
	drawTableSurface();
	drawPlayground();
	drawPlaygroundBorder();
}

function drawTableSurface(){
	gcc.fillStyle = COLOR.TABLE;
	gcc.fillRect(0, 0, gcc.canvas.width, gcc.canvas.height);
}

function drawPlayground(){
	gcc.fillStyle = COLOR.PLAYGROUND;
	gcc.fillRect(0-CAMERA.x, 0-CAMERA.y, GAME.TABLESIZE, GAME.TABLESIZE);
}

function drawPlaygroundBorder(){
	var borderWidth = 2;
	gcc.strokeStyle = COLOR.PLAYGROUNDBORDER;
	gcc.lineWidth = borderWidth;
	gcc.strokeRect(0-CAMERA.x-borderWidth, 0-CAMERA.y-borderWidth, GAME.TABLESIZE+2*borderWidth, GAME.TABLESIZE+2*borderWidth);
}

function drawSpawns(){
	gcc.fillStyle = COLOR.SPAWN;
	gcc.fillRect(inchToPixels(8)-CAMERA.x, 0-CAMERA.y, GAME.TABLESIZE-inchToPixels(2*GAME.SPAWN.FROMSIDE), inchToPixels(GAME.SPAWN.FROMBACK));
	gcc.fillRect(inchToPixels(8)-CAMERA.x, GAME.TABLESIZE-inchToPixels(3)-CAMERA.y, GAME.TABLESIZE-inchToPixels(2*GAME.SPAWN.FROMSIDE), inchToPixels(GAME.SPAWN.FROMBACK));
}

function drawMovedLines(){
	for(var army in GAMESTATE["armies"]){
		for(var warrior in GAMESTATE["armies"][army]){
			warrior = GAMESTATE["armies"][army][warrior];
			drawMovedLine(warrior);
		}
	}
}

function drawMovedLine(warrior){
	gcc.strokeStyle = COLOR.BLACK;
	gcc.lineWidth = 2;
	gcc.beginPath();
	gcc.moveTo(warrior.oldPosition.x - CAMERA.x, warrior.oldPosition.y - CAMERA.y);
	gcc.lineTo(warrior.position.x - CAMERA.x, warrior.position.y - CAMERA.y);
	gcc.stroke();
}

function drawWarriors(){
	for(var army in GAMESTATE["armies"]){
		for(var warrior in GAMESTATE["armies"][army]){
			warrior = GAMESTATE["armies"][army][warrior];
			drawWarrior(warrior, "position");
			if(movingWarrior){
				gcc.globalAlpha = 0.75;
				drawMoveLine(warrior)
				drawWarrior(warrior, "drawPosition");
				gcc.globalAlpha = 1.0;
			}
			if(isInList(warrior, freeRotationsFor)){
				drawFreeRotationIndicator(warrior);
			}
		}
	}
}

function drawSelectedWarrior(){
	if(selectedWarrior!=null){
		drawMovementRange(selectedWarrior);
		drawAttackRange(selectedWarrior);
		drawSelectedIndicator(selectedWarrior);
		drawContactLines(selectedWarrior);
		drawRangeLines(selectedWarrior);
		//drawDirection(selectedWarrior, "position");
		//drawDirection(selectedWarrior, "drawPosition");
	}
}

function drawWarrior(warrior, pos){
	gcc.save();
	if(pos=="position"){
		gcc.translate(warrior.position.x - CAMERA.x, warrior.position.y - CAMERA.y);
		gcc.rotate(warrior.position.rotation*Math.PI/180);
	} else if(pos=="drawPosition"){
		gcc.translate(warrior.drawPosition.x - CAMERA.x, warrior.drawPosition.y - CAMERA.y);
		gcc.rotate(warrior.drawPosition.rotation*Math.PI/180);
	}
	drawBase(warrior);
	drawFrontArc(warrior);
	drawRearArc(warrior);
	drawGotActionIndicator(warrior);
	drawActions(warrior);
	drawOwnerIndicator(warrior);
	gcc.restore();
	if(PLAYER.isAI) drawDirection(warrior, "position");
}

function drawBase(warrior){
	gcc.beginPath();
	gcc.fillStyle = (yourWarrior(warrior)) ? COLOR.ALLYBLACK : COLOR.FOEBLACK;
	gcc.lineWidth = 1;
	gcc.arc(0, 0, warrior.baseradius, 0, 2*Math.PI);
	gcc.fill();
}

function drawFrontArc(warrior){
	var startAngle = -(warrior.frontarc*15)*(Math.PI/180);
	var endAngle = (warrior.frontarc*15)*(Math.PI/180);
	gcc.strokeStyle = COLOR.FRONTARC;

	gcc.beginPath();
	gcc.lineWidth = 1;
	gcc.moveTo(0,0);
	gcc.lineTo(Math.cos(startAngle)*warrior.baseradius, Math.sin(startAngle)*warrior.baseradius);
	gcc.stroke();

	gcc.beginPath();
	gcc.lineWidth = 6;
	gcc.arc(0, 0, warrior.baseradius-(gcc.lineWidth/2), startAngle, endAngle);
	gcc.stroke();

	gcc.beginPath();
	gcc.lineWidth = 1;
	gcc.moveTo(0,0);
	gcc.lineTo(Math.cos(endAngle)*warrior.baseradius, Math.sin(endAngle)*warrior.baseradius);
	gcc.stroke();
}

function drawRearArc(warrior){
	var startAngle = (warrior.reararc*15+90)*(Math.PI/180);
	var endAngle = -(warrior.reararc*15+90)*(Math.PI/180);
	gcc.strokeStyle = COLOR.REARARC;
	gcc.lineWidth = 6;
	gcc.beginPath();
	gcc.arc(0, 0, warrior.baseradius-(gcc.lineWidth/2), startAngle, endAngle);
	gcc.stroke();
}

function drawGotActionIndicator(warrior){
	if(warrior.gotAction){
		gcc.fillStyle = COLOR.BLACK;
		gcc.globalAlpha = 0.50;
		gcc.beginPath();
		gcc.arc(0, 0, warrior.baseradius, 0, 2*Math.PI);
		gcc.fill();
		gcc.globalAlpha = 1.0;
	}
}

function drawOwnerIndicator(warrior){
	gcc.beginPath();
	gcc.fillStyle = (yourWarrior(warrior)) ? COLOR.ALLY : COLOR.FOE;
	gcc.lineWidth = 1;
	gcc.arc(0, 0, 4, 0, 2*Math.PI);
	gcc.fill();
}

function drawActions(warrior){
	if(warrior.actions==1){
		gcc.beginPath();
		gcc.fillStyle = COLOR.ACTIONTOKEN;
		gcc.arc(0-warrior.baseradius/2, 0, 4, 0, 2*Math.PI);
		gcc.fill();
	} else if(warrior.actions==2){
		gcc.beginPath();
		gcc.fillStyle = COLOR.ACTIONTOKEN;
		gcc.arc(0-warrior.baseradius/2, 0-8, 4, 0, 2*Math.PI);
		gcc.fill();
		gcc.beginPath();
		gcc.fillStyle = COLOR.ACTIONTOKEN;
		gcc.arc(0-warrior.baseradius/2, 0+8, 4, 0, 2*Math.PI);
		gcc.fill();
	}
}

function drawDirection(warrior, pos){
	gcc.strokeStyle = COLOR.DIRECTION;
	gcc.lineWidth = 2;
	gcc.beginPath();
	if(pos=="position"){
		gcc.moveTo(warrior.position.x - CAMERA.x, warrior.position.y - CAMERA.y);
		gcc.lineTo((warrior.position.x + (warrior.position.direction.x*warrior.baseradius)) - CAMERA.x, (warrior.position.y + (warrior.position.direction.y*warrior.baseradius)) - CAMERA.y);
	} else if(pos=="drawPosition"){
		gcc.moveTo(warrior.drawPosition.x - CAMERA.x, warrior.drawPosition.y - CAMERA.y);
		gcc.lineTo((warrior.drawPosition.x + (warrior.drawPosition.direction.x*warrior.baseradius)) - CAMERA.x, (warrior.drawPosition.y + (warrior.drawPosition.direction.y*warrior.baseradius)) - CAMERA.y);
	}
	gcc.stroke();
}

function drawMoveLine(warrior){
	gcc.strokeStyle = COLOR.BLACK;
	gcc.lineWidth = 2;
	gcc.beginPath();
	gcc.moveTo(warrior.position.x - CAMERA.x, warrior.position.y - CAMERA.y);
	gcc.lineTo(warrior.drawPosition.x - CAMERA.x, warrior.drawPosition.y - CAMERA.y);
	gcc.stroke();
}

function drawContactLines(warrior){
	gcc.lineWidth = 2;
	for(var i=0; i<warrior.newBaseContacts.length; i++){
		if(checkInSight(warrior, GAMESTATE["armies"][warrior.newBaseContacts[i].owner][warrior.newBaseContacts[i].index], "drawPosition")){
			gcc.strokeStyle = COLOR.GREEN;
		} else {
			gcc.strokeStyle = COLOR.RED;
		}
		gcc.beginPath();
		gcc.moveTo(warrior.drawPosition.x - CAMERA.x, warrior.drawPosition.y - CAMERA.y);
		gcc.lineTo(GAMESTATE["armies"][warrior.newBaseContacts[i].owner][warrior.newBaseContacts[i].index].position.x - CAMERA.x, GAMESTATE["armies"][warrior.newBaseContacts[i].owner][warrior.newBaseContacts[i].index].position.y - CAMERA.y);
		gcc.stroke();
	}
	for(var i=0; i<warrior.baseContacts.length; i++){
		if(checkInSight(warrior, GAMESTATE["armies"][warrior.baseContacts[i].owner][warrior.baseContacts[i].index], "position")){
			gcc.strokeStyle = COLOR.GREEN;
		} else {
			gcc.strokeStyle = COLOR.RED;
		}
		gcc.beginPath();
		gcc.moveTo(warrior.position.x - CAMERA.x, warrior.position.y - CAMERA.y);
		gcc.lineTo(GAMESTATE["armies"][warrior.baseContacts[i].owner][warrior.baseContacts[i].index].position.x - CAMERA.x, GAMESTATE["armies"][warrior.baseContacts[i].owner][warrior.baseContacts[i].index].position.y - CAMERA.y);
		gcc.stroke();
	}
}

function drawRangeLines(warrior){
	gcc.lineWidth = 2;
	for(var i=0; i<warrior.rangeTargets.length; i++){
		gcc.strokeStyle = COLOR.GREEN;
		gcc.beginPath();
		gcc.moveTo(warrior.position.x - CAMERA.x, warrior.position.y - CAMERA.y);
		gcc.lineTo(GAMESTATE["armies"][warrior.rangeTargets[i].owner][warrior.rangeTargets[i].index].position.x - CAMERA.x, GAMESTATE["armies"][warrior.rangeTargets[i].owner][warrior.rangeTargets[i].index].position.y - CAMERA.y);
		gcc.stroke();
	}
}

function drawMovementRange(warrior){
	gcc.strokeStyle = COLOR.MOVEMENTRANGE;
	gcc.fillStyle = COLOR.BLACK;
	gcc.globalAlpha = 0.25;
	gcc.beginPath();
	gcc.arc(warrior.position.x-CAMERA.x, warrior.position.y-CAMERA.y, inchToPixels(warrior.activeState.movementrange)+warrior.baseradius, 0, 2*Math.PI);
	gcc.fill();
	gcc.globalAlpha = 1.0;
	gcc.beginPath();
	gcc.arc(warrior.position.x-CAMERA.x, warrior.position.y-CAMERA.y, inchToPixels(warrior.activeState.movementrange)+warrior.baseradius, 0, 2*Math.PI);
	gcc.stroke();
}

function drawAttackRange(warrior){
	gcc.strokeStyle = COLOR.ATTACKRANGE;
	gcc.fillStyle = COLOR.BLACK;
	gcc.globalAlpha = 0.25;
	gcc.beginPath();
	gcc.arc(warrior.position.x-CAMERA.x, warrior.position.y-CAMERA.y, inchToPixels(warrior.range), 0, 2*Math.PI);
	gcc.fill();
	gcc.globalAlpha = 1.0;
	gcc.beginPath();
	gcc.arc(warrior.position.x-CAMERA.x, warrior.position.y-CAMERA.y, inchToPixels(warrior.range), 0, 2*Math.PI);
	gcc.stroke();
}

function drawSelectedIndicator(warrior){
	if(warrior.owner==PLAYER.id){
		gcc.strokeStyle = COLOR.SELECTEDALLY;
	} else {
		gcc.strokeStyle = COLOR.SELECTEDFOE;
	}
	gcc.beginPath();
	gcc.lineWidth = 2;
	gcc.arc(warrior.position.x-CAMERA.x, warrior.position.y-CAMERA.y, warrior.baseradius-(gcc.lineWidth/2), 0, 2*Math.PI);
	gcc.stroke();
}

function drawFreeRotationIndicator(warrior){
	gcc.fillStyle = COLOR.ROTATION;
	gcc.globalAlpha = 0.5;
	gcc.beginPath();
	gcc.lineWidth = 2;
	gcc.arc(warrior.position.x-CAMERA.x, warrior.position.y-CAMERA.y, warrior.baseradius-(gcc.lineWidth/2), 0, 2*Math.PI);
	gcc.fill();
	gcc.globalAlpha = 1.0;
}

function drawControlPoints(){
	for(var cp in GAMESTATE["controlpoints"]){
		CP = GAMESTATE["controlpoints"][cp];
		gcc.beginPath();
		if(CP.owner == null){
			gcc.fillStyle = COLOR.WHITE;
		} else if(yourCP(CP)){
			gcc.fillStyle = COLOR.ALLY;
		} else {
			gcc.fillStyle = COLOR.FOE;
		}
		gcc.arc(CP.position.x-CAMERA.x, CP.position.y-CAMERA.y, CP.radius, 0, 2*Math.PI);
		gcc.fill();

		gcc.beginPath();
		gcc.strokeStyle = COLOR.BLACK;
		gcc.lineWidth = 4;
		gcc.arc(CP.position.x-CAMERA.x, CP.position.y-CAMERA.y, CP.radius-(gcc.lineWidth/2), 0, 2*Math.PI);
		gcc.stroke();

		gcc.fillStyle = COLOR.BLACK;
		gcc.font = CP.radius+"pt Tahoma";
		gcc.fillText(CP.name, (CP.position.x-(gcc.measureText(CP.name).width/2))-CAMERA.x, (CP.position.y+(CP.radius/2))-CAMERA.y);
	}
}

function screenshot(png){
	gcc.canvas.width = 2000;
	gcc.canvas.height = 2000;
	setCameraZero();
	SCREENSHOTING = true;
	updateCanvas();
	var screenshot  =  (png) ? gc.toDataURL("image/png") : gc.toDataURL("image/jpeg", 0.1);
	gcc.canvas.width = window.innerWidth;
	gcc.canvas.height = window.innerHeight;
	restoreCamera();
	SCREENSHOTING = false;
	updateCanvas();
	return screenshot;
}

function downloadScreenshot(filename){
	var a = $("<a>")
	    .attr("href", screenshot(true))
	    .attr("download", filename+".png")
	    .appendTo("body");
	a[0].click();
	a.remove();
}

function getScreenshotFilename(){
	return "mko_screenshot_"+GAMESTATE.seed+"_"+GAMESTATE.turn+"_("+GAMESTATE.activePlayer+")";
}
