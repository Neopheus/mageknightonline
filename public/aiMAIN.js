var SIMULATING = false;
var SIMULATINGGAME = false;

var SCRIPTS = [
  scriptRest,
  scriptHold,
  scriptCapture,
  scriptMelee,
  scriptRange,
  scriptKite,
  scriptRetreat
];

var SCRIPTSSMALL = [
  scriptRest,
  scriptCapture,
  scriptMelee,
  scriptKite,
  scriptRetreat
];

var random = null;
var actions = null;

function ai(){
  socket.emit("aiStillAlive");
  if((PLAYER.isAI || SIMULATINGGAME) && !GAMESTATE.gameover){
    if(placeWarriors){
      scriptPlacingArmy();
    } else if(yourTurn() && !GAMESTATE.gameover){
      if(actions===null){
        setCameraZero();
        if(PLAYER.isAI==="random" || SIMULATINGGAME){
          actions = randomReasoning();
        } else if(PLAYER.isAI==="ga"){
          actions = ga();
        } else if(PLAYER.isAI==="mcts"){
          actions = mcts();
        }
      }
      executeNextAction();
    } else if(opponentsTurn() && freeRotation){
      var warriorToSpin = getWarrior(freeRotationsFor[0].owner, freeRotationsFor[0].index);
      //TODO check if its good to rotate: if(good) aiFreeSpin else aiAbortSpins
      aiFreeSpin(warriorToSpin, warriorToFace);
    }
  }
}

function executeActions(){
  for(var i in actions){
    var owner = actions[i].owner;
    var index = actions[i].index;
    var script = actions[i].script;
    var warrior = getWarrior(owner, index);
    if(!GAMESTATE.gameover) script(warrior);
  }
}

function executeNextAction(){
  if(actions.length>0){
    if(!GAMESTATE.gameover){
      var action = actions.shift();
      var owner = action.owner;
      var index = action.index;
      var script = action.script;
      var warrior = getWarrior(owner, index);
      script(warrior);
    }
  } else {
    aiEndTurn();
  }
}

function randomReasoning(){
  var actions = [];
  for(var i in GAMESTATE["armies"][PLAYER.id]){
    var action = {
      owner: PLAYER.id,
      index: i,
      script: getRandomScript()
    };
    actions.push(action);
  }
  shuffleArray(actions);
  return actions;
}

//###############
//### SCRIPTS ###
//###############

//rest: warrior is doing nothing
function scriptRest(warrior){
  if(!SIMULATING) console.log("scriptRest("+warrior.name+")", warrior.id, warrior.index);
  executeNextAction();
}

//hold: warrior trys to hold the current position (CP)
function scriptHold(warrior){
  if(!SIMULATING) console.log("scriptHold("+warrior.name+")", warrior.id, warrior.index);
  if(isActionPossible(warrior)){
    var nextCP = getNextCPtoHold(warrior);
    if(!SIMULATING) console.log("###", warrior, nextCP);
    if(nextCP){
      var dist = distance(warrior.position, nextCP.position);
      if(dist > inchToPixels(warrior.activeState.movementrange)+warrior.baseradius+nextCP.radius){
        aiMoveTowards(warrior, nextCP.position, "nextEnemy");
      } else {
        var bestPos = getRandomPosOnCP(nextCP);
        var nextEnemy = getNextEnemyFromPos(bestPos);
        var lookAt = {
          x: nextEnemy.position.x,
          y: nextEnemy.position.y
        };
        aiMoveTo(warrior, bestPos, lookAt);
      }
    } else {
      if(!SIMULATING) console.log("NoCpToHold");
      executeNextAction();
    }
  } else {
    if(!SIMULATING) console.log("actionNotPossible");
    executeNextAction();
  }
}

//capture: warrior moves on or at least towards a CP to capture it
function scriptCapture(warrior){
  if(!SIMULATING) console.log("scriptCapture("+warrior.name+")", warrior.id, warrior.index);
  if(isActionPossible(warrior)){
    var nextCP = getNextCPtoCapture(warrior);
    if(!SIMULATING) console.log("###", warrior, nextCP);
    if(nextCP){
      var dist = distance(warrior.position, nextCP.position);
      if(dist > inchToPixels(warrior.activeState.movementrange)+warrior.baseradius+nextCP.radius){
        aiMoveTowards(warrior, nextCP.position, "nextEnemy");
      } else {
        var bestPos = getRandomPosOnCP(nextCP);
        var nextEnemy = getNextEnemyFromPos(bestPos);
        var lookAt = {
          x: nextEnemy.position.x,
          y: nextEnemy.position.y
        };
        aiMoveTo(warrior, bestPos, lookAt);
      }
    } else {
      if(!SIMULATING) console.log("noCpToCap");
      executeNextAction();
    }
  } else {
    if(!SIMULATING) console.log("actionNotPossible");
    executeNextAction();
  }
}

//melee:
function scriptMelee(warrior){
  if(!SIMULATING) console.log("scriptMelee("+warrior.name+")", warrior.id, warrior.index);
  var target = null;
  if(isActionPossible(warrior)){
    if(canMelee(warrior)){
      target = getWeakestTarget(warrior, getMeleeTargets(warrior));
      aiMelee(warrior, target);
    } else if(isMovable(warrior)){
      target = getNextEnemyFromPos(warrior.position);
      var dist = distance(warrior.position, target.position)
      if(dist > inchToPixels(warrior.activeState.movementrange)+warrior.baseradius+target.baseradius){
        aiMoveTowards(warrior, target.position, target.position);
      } else {
        var goTo = getContactPos(warrior, target);
        var lookAt = target.position;
        aiMoveTo(warrior, goTo, lookAt);
      }
    } else {
      if(!SIMULATING) console.log("can't move");
      executeNextAction();
    }
  } else {
    if(!SIMULATING) console.log("actionNotPossible");
    executeNextAction();
  }
}

//range:
function scriptRange(warrior){
  if(!SIMULATING) console.log("scriptRange("+warrior.name+")", warrior.id, warrior.index);
  if(isActionPossible(warrior) && warrior.range>0){
    if(canRange(warrior)){
      target = getWeakestTarget(warrior, getRangeTargets(warrior));
      aiRange(warrior, target);
    } else if(isMovable(warrior)){
      target = getNextEnemyFromPos(warrior.position);
      var dist = distance(warrior.position, target.position);
      if(dist > inchToPixels(warrior.activeState.movementrange+warrior.range)){
        aiMoveTowards(warrior, target.position, target.position);
      } else {
        var goTo = getRangePos(warrior, target);
        var lookAt = target.position;
        aiMoveTo(warrior, goTo, lookAt);
      }
    } else {
      if(!SIMULATING) console.log("can't move");
      executeNextAction();
    }
  } else {
    if(!SIMULATING) console.log("actionNotPossible");
    if(!SIMULATING) if(warrior.range<=0) console.log("not range")
    executeNextAction();
  }
}

//kite:
function scriptKite(warrior){
  if(!SIMULATING) console.log("scriptKite("+warrior.name+")", warrior.id, warrior.index);
  if(isActionPossible(warrior) && warrior.range>0){
    var kiteTarget = getKiteTarget(warrior);
    if(kiteTarget){
      var dist = getDistance(warrior, kiteTarget);
      if(canRange(warrior) && dist>inchToPixels(kiteTarget.activeState.movementrange)+kiteTarget.baseradius+warrior.baseradius){
        aiRange(warrior, kiteTarget);
      } else if(isMovable(warrior)){
        if(dist>inchToPixels(warrior.activeState.movementrange+warrior.range)){
          aiMoveTowards(warrior, kiteTarget.position, kiteTarget.position);
        } else {
          var goTo = getRangePos(warrior, kiteTarget);
          var lookAt = kiteTarget.position;
          aiMoveTo(warrior, goTo, lookAt);
        }
      } else {
        if(!SIMULATING) console.log("can't move");
        executeNextAction();
      }
    } else {
      if(!SIMULATING) console.log("noKiteTargets");
      executeNextAction();
    }
  } else {
    if(!SIMULATING) console.log("actionNotPossible");
    if(!SIMULATING) if(warrior.range<=0) console.log("not range")
    executeNextAction();
  }
}

//retreat:
function scriptRetreat(warrior){
  if(!SIMULATING) console.log("scriptRetreat("+warrior.name+")", warrior.id, warrior.index);
  if(isActionPossible(warrior)){
    var dangerousEnemies = getDangerousEnemies(warrior);
    if(dangerousEnemies.length>0){
      var goTo = getRetreatPos(warrior, dangerousEnemies);
      var lookAt = getNextEnemyFromPos(goTo).position;
      aiMoveTo(warrior, goTo, lookAt);
    } else {
      if(!SIMULATING) console.log("noDangerousEnemies");
      executeNextAction();
    }
  } else {
    if(!SIMULATING) console.log("actionNotPossible");
    executeNextAction();
  }
}

//placingArmy: to place all warriors on the table at the beginning of the game
function scriptPlacingArmy(){
  setCameraZero();
  console.log("scriptPlacingArmy()", GAMESTATE["armies"][PLAYER.id]);
  for(var warriorIndex in GAMESTATE["armies"][PLAYER.id]){
    var warrior = getWarrior(PLAYER.id, warriorIndex);
    var p1 = PLAYER.id===GAMESTATE.p1;
    var minX = 0+inchToPixels(8);
    var maxX = GAME.TABLESIZE-inchToPixels(8);
    var minY = (p1) ? 0+warrior.baseradius : GAME.TABLESIZE-inchToPixels(3);
    var maxY = (p1) ? 0+inchToPixels(3) : GAME.TABLESIZE-warrior.baseradius;
    var invalidPos = true;
    while(invalidPos){
      var goTo = {
        x: Math.floor(random() * (maxX - minX +1)) + minX,
        y: Math.floor(random() * (maxY - minY +1)) + minY
      };
      lookAt = getNextCPfromPos(goTo).position;
      invalidPos = wouldIntersectWithWarriors(warrior, goTo);
    }
    aiMoveTo(warrior, goTo, lookAt);
  }
  socket.emit("sendOwnArmyPositions", getArmyPositions(PLAYER.id));
  $("#placedButton").hide();
  restoreCamera();
}

//###############
//### ACTIONS ###
//###############

//move warrior towards a object
function aiMoveTowards(warrior, towards, lookAt){
  var v = {
    x: towards.x-warrior.position.x,
    y: towards.y-warrior.position.y
  }
  var dist = Math.hypot(v.x, v.y);
  v.x = v.x/dist*inchToPixels(warrior.activeState.movementrange)+warrior.position.x;
  v.y = v.y/dist*inchToPixels(warrior.activeState.movementrange)+warrior.position.y;
  if(lookAt==="nextEnemy"){
    lookAt = getNextEnemyFromPos(v).position;
  }
  aiMoveTo(warrior, v, lookAt);
}

//execute a move action on the warrior to the position pos facing into direction dir
function aiMoveTo(warrior, pos, dir){
  warriorToMove = warrior;
  moveWarrior({pageX: pos.x, pageY: pos.y});
  rotateWarrior({pageX: dir.x, pageY: dir.y});
  if(placeWarriors || SIMULATING){
    moveTo(warriorToMove, warriorToMove.drawPosition, warriorToMove.baseContacts);
    if(!placeWarriors) ai();
  } else {
    socket.emit("callMoveTo", compressWarrior(warriorToMove), warriorToMove.drawPosition, warriorToMove.newBaseContacts);
  }
}

//
function aiMelee(attacker, defender){
  if(SIMULATING){
    fightClose(attacker, defender);
    ai();
  } else {
    socket.emit("callFightClose", compressWarrior(attacker), compressWarrior(defender));
  }
}

//
function aiRange(attacker, defender){
  if(SIMULATING){
    fightRange(attacker, [compressWarrior(defender)]);
    ai();
  } else {
    socket.emit("callFightRange", compressWarrior(attacker), [compressWarrior(defender)]);
  }
}

//
function aiFreeSpin(warrior, target){
  aiMoveTo(warrior, warrior.position, target.position);
}

//
function aiAbortSpins(){
  if(SIMULATING){
    abortSpins();
  } else {
    socket.emit("abortSpins");
  }
}

//
function aiEndTurn(){
  actions = null;
  if(SIMULATING){
    endTurn();
    ai();
  } else {
    socket.emit("callEndTurn");
  }
  restoreCamera();
  //console.log("aiEndTurn");
}

//##############
//### HELPER ###
//##############

function getRandomScript(){
  return SCRIPTS[Math.floor(random()*SCRIPTS.length)];
}

function getRandomScriptSmall(){
  return SCRIPTSSMALL[Math.floor(random()*SCRIPTSSMALL.length)];
}

//get a position to get in contact with target
function getContactPos(warrior, target){
  v = {
    x: target.position.x-warrior.position.x,
    y: target.position.y-warrior.position.y
  };
  var dist = Math.hypot(v.x, v.y);
  v.x = ((v.x/dist)*(dist-10))+warrior.position.x;
  v.y = ((v.y/dist)*(dist-10))+warrior.position.y;
  return v;
}

//get a position to get in range for fight
function getRangePos(warrior, target){
  v = {
    x: target.position.x-warrior.position.x,
    y: target.position.y-warrior.position.y
  };
  var dist = Math.hypot(v.x, v.y);
  v.x = ((v.x/dist)*(dist-inchToPixels(warrior.range)+1))+warrior.position.x;
  v.y = ((v.y/dist)*(dist-inchToPixels(warrior.range)+1))+warrior.position.y;
  return v;
}

//get the nearest enemy from the given position
function getNextEnemyFromPos(pos){
  var nextEnemy = null;
  var bestDist = Number.MAX_VALUE;
  for(var enemyIndex in GAMESTATE["armies"][OPPONENT.id]){
    var enemy = getWarrior(OPPONENT.id, enemyIndex);
    var dist = distance(pos, enemy.position);
    if(dist < bestDist){
      bestDist = dist;
      nextEnemy = enemy;
    }
  }
  return nextEnemy;
}

//
function getDangerousEnemies(warrior){
  var dangerousEnemies = [];
  for(var enemy in GAMESTATE["armies"][OPPONENT.id]){
    enemy = GAMESTATE["armies"][OPPONENT.id][enemy];
    if(!isDemoralized(enemy)){
      var inRangeDist = checkInRange(enemy, warrior) && checkFreeSight(enemy, warrior) && !isInList(warrior, enemy.baseContacts);
      var inMeleeDist = getDistance(warrior, enemy)<=inchToPixels(enemy.activeState.movementrange)+warrior.baseradius+enemy.baseradius;
      if(inRangeDist || inMeleeDist) dangerousEnemies.push(enemy);
    }
  }
  return dangerousEnemies;
}

//
function getRetreatPos(warrior, enemies){
  var pos = {
    x: warrior.position.x,
    y: warrior.position.y
  };
  var strongestEnemy = getStrongestTarget(warrior, enemies);
  if(strongestEnemy){
    var v = {
      x: warrior.position.x-strongestEnemy.position.x,
      y: warrior.position.x-strongestEnemy.position.y
    };
    var dist = Math.hypot(v.x, v.y);
    pos.x = warrior.position.x+((v.x/dist)*inchToPixels(warrior.activeState.movementrange));
    pos.y = warrior.position.y+((v.y/dist)*inchToPixels(warrior.activeState.movementrange));
  }
  return pos;
}

//get the nearest CP that is not under your control
function getNextCPtoCapture(warrior){
  var nextCP = null;
  var bestDist = Number.MAX_VALUE;
  for(var cp in GAMESTATE.controlpoints){
    cp = GAMESTATE.controlpoints[cp];
    var dist = distance(warrior.position, cp.position);
    if(dist<bestDist&&cp.owner!=warrior.owner){
      bestDist = dist;
      nextCP = cp;
    }
  }
  return nextCP;
}

//get the nearest CP from position
function getNextCPfromPos(pos, gamestate){
  gamestate = (gamestate) ? gamestate : GAMESTATE;
  var nextCP = null;
  var bestDist = Number.MAX_VALUE;
  for(var cp in gamestate.controlpoints){
    cp = gamestate.controlpoints[cp];
    var dist = distance(pos, cp.position);
    if(dist<bestDist){
      bestDist = dist;
      nextCP = cp;
    }
  }
  return nextCP;
}

//get the nearest CP that is under your control
function getNextCPtoHold(warrior){
  var nextCP = null;
  var bestDist = Number.MAX_VALUE;
  for(var cp in GAMESTATE.controlpoints){
    cp = GAMESTATE.controlpoints[cp];
    var dist = distance(warrior.position, cp.position);
    if(dist<bestDist&&cp.owner===warrior.owner){
      bestDist = dist;
      nextCP = cp;
    }
  }
  return nextCP;
}

function getRandomPosOnCP(cp){
  var min = -(cp.radius-1);
  var max = cp.radius-1;
  var ranX = Math.floor(random() * (max - min +1)) + min;
  var ranY = Math.floor(random() * (max - min +1)) + min;
  var posOnCP = {
    x: cp.position.x+ranX,
    y: cp.position.y+ranY
  };
  return posOnCP;
}

//get distance between two positions
function distance(posA, posB){
  return Math.hypot(posB.x-posA.x, posB.y-posA.y);
}

//get the probability of rolling at least the given number
function getDiceProbability(number){
	var hits = 0;
	var counter = 0;
	for(var i=1; i<=6; i++){
		for(var j=1; j<=6; j++){
			if(i+j>=number) hits++;
			counter++
		}
	}
	return hits/counter
}

function saveCamera(){
  CAMERA.oldX = CAMERA.x;
  CAMERA.oldY = CAMERA.y;
}

function restoreCamera(){
  CAMERA.x = CAMERA.oldX;
  CAMERA.y = CAMERA.oldY;
}

function setCameraZero(){
  saveCamera();
  CAMERA.x = 0;
  CAMERA.y = 0;
}

function saveGamestate(){
  SAVEDGAMESTATE = cloneObject(GAMESTATE);
}

function restoreGamestate(){
  GAMESTATE = cloneObject(SAVEDGAMESTATE);
}

function saveUser(){
  SAVEDPLAYER = cloneObject(PLAYER);
  SAVEDOPPONENT = cloneObject(OPPONENT);
}

function restoreUser(){
  PLAYER = cloneObject(SAVEDPLAYER);
  OPPONENT = cloneObject(SAVEDOPPONENT);
}

function swapUser(){
  var player = cloneObject(PLAYER);
  var opponent = cloneObject(OPPONENT);
  PLAYER = opponent;
  OPPONENT = player;
}

function cloneObject(obj){
	var clone;
	if(obj instanceof Array){
		clone = [];
		for(var i=0; i<obj.length; i++){
			clone[i] = cloneObject(obj[i]);
		}
		return clone;
	} else if(obj instanceof Object){
		clone = {};
		for(var attr in obj){
			clone[attr] = cloneObject(obj[attr]);
		}
		return clone;
	} else {
		return obj;
	}
}

//random function to not use Math.random(), that would cause problems with the seed
var random = new Math.seedrandom(Date.now());
