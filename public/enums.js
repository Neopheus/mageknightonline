var GAME = {
	TABLESIZE: 2000,
	SPAWN: {
		FROMSIDE: 8,
		FROMBACK: 3
	},
	MAXARMYSIZE: 300,
	ROUNDSTOWIN: 3
};

var POSITION = {
	NORMAL: "position",
	DRAW: "drawPosition"
};

var MESSAGE = {
	WARRIORUNIQUE: "Warrior is unique and already in army.",
	ARMYEMPTY: "No warriors in army!",
	ARMYTOBIG: "Armysize would be to high.",
	MOVED: function(warrior){
		var color = COLOR.BLUE;
		if(yourWarrior(warrior)){
			color = COLOR.ALLY;
		} else {
			color = COLOR.FOE;
		}
		return "<font color="+color+">" + warrior.name+"</font> was moved.";
	},
	SPINNED: function(warrior){
		var color = COLOR.BLUE;
		if(yourWarrior(warrior)){
			color = COLOR.ALLY;
		} else {
			color = COLOR.FOE;
		}
		return "<font color="+color+">" + warrior.name+"</font> was spinned.";
	},
	FREESPINS: function(yourTurn){
		if(yourTurn){
			return "Free spins for opponent: "+freeRotationsFor.length+" spins left.";
		} else {
			return message = "Free spins for you: "+freeRotationsFor.length+" spins left.";
		}
	},
	SPINSAPPLIED: "All spins applied.",
	SPINSCANCLED: "Remaining spins cancled.",
	PUSHINGDMG: function(warrior){
		var color = COLOR.BLUE;
		if(yourWarrior(warrior)){
			color = COLOR.ALLY;
		} else {
			color = COLOR.FOE;
		}
		return "<font color="+color+">" + warrior.name+"</font> takes pushing damage.";
	},
	REARATTACK: "Attack from behind: +1 to attackvalue.",
	DICEROLL: function(diceValue, yourTurn){
		if(yourTurn){
			return "You rolled "+diceValue+".";
		} else {
			return "Opponent rolled "+diceValue+".";
		}
	},
	ATTACK: function(attacker, defender, atkValue, defValue){
		if(yourWarrior(attacker)){
			return "<font color="+COLOR.ALLY+">"+attacker.name+"</font> attacks with "+atkValue+"atk against <font color="+COLOR.FOE+">"+defender.name+"</font> with "+defValue+"def.";
		} else {
			return "<font color="+COLOR.FOE+">"+attacker.name+"</font> attacks with "+atkValue+"atk against <font color="+COLOR.ALLY+">"+defender.name+"</font> with "+defValue+"def.";
		}
	},
	CRITICALHIT: "It was a crittical hit, +1 to damage.",
	HIT: "It was a hit.",
	NOHIT: "It was no hit.",
	CRITICALMISS: function(attacker){
		if(yourWarrior(attacker)){
			return "You rolled two times 1: Critical miss and no hit.";
		} else {
			return "Opponent rolled two times 1: Critical miss and no hit.";
		}
	},
	DMG: function(warrior, dmg){
		var color = COLOR.BLUE;
		if(yourWarrior(warrior)){
			color = COLOR.ALLY;
		} else {
			color = COLOR.FOE;
		}
		return "<font color="+color+">" + warrior.name+"</font> takes "+dmg+" damage.";
	},
	NOTDEAD: function(warrior){
		var color = COLOR.BLUE;
		if(yourWarrior(warrior)){
			color = COLOR.ALLY;
		} else {
			color = COLOR.FOE;
		}
		return "<font color="+color+">" + warrior.name+"</font> is not dead, " + (warrior.life - warrior.damageTaken) + "hp left.";
	},
	DEAD: function(warrior){
		var color = COLOR.BLUE;
		if(yourWarrior(warrior)){
			color = COLOR.ALLY;
		} else {
			color = COLOR.FOE;
		}
		return "<font color="+color+">" + warrior.name+"</font> is dead."
	},
	STARTGAME: function(yourTurn){
		if(yourTurn){
			return "You start!";
		} else {
			return "Opponent starts!";
		}
	},
	ENDTURN: function(yourTurn){
		if(yourTurn){
			return "Your turn...";
		} else {
			return "Opponents turn...";
		}
	},
	BREAKFREE: function(warrior){
		var color = COLOR.BLUE;
		if(yourWarrior(warrior)){
			color = COLOR.ALLY;
		} else {
			color = COLOR.FOE;
		}
		return "<font color="+color+">" + warrior.name+"</font> breaks free and can move.";
	},
	BREAKFREEFAILED: function(warrior){
		var color = COLOR.BLUE;
		if(yourWarrior(warrior)){
			color = COLOR.ALLY;
		} else {
			color = COLOR.FOE;
		}
		return "Breaking free failed, <font color="+color+">" + warrior.name+"</font> can only spin.";
	},
	YOUCAPPED: function(CP){
		if(opponentsCP(CP)){
			 return "You capped controlpoint "+CP.name+" from opponent.";
		} else if(!opponentsCP(CP) && !yourCP(CP)){
			return "You capped controlpoint "+CP.name+".";
		}
	},
	OPPONENTCAPPED: function(CP){
		if(yourCP(CP)){
			return "Opponent capped controlpoint "+CP.name+" from you.";
		} else if(!opponentsCP(CP) && !yourCP(CP)){
			return "Opponent capped controlpoint "+CP.name+".";
		}
	},
	CONTESTED: function(CP){
		return "Controlpoint "+CP.name+" is contested.";
	}
};

var COLOR = {
	BLACK: "#000000",
	WHITE: "#FFFFFF",
	RED: "#FF0000",
	GREEN: "#00FF00",
	BLUE: "#0000FF",
	TABLE: "#663300",
	PLAYGROUND: "#287728",
	PLAYGROUNDBORDER: "#000000",
	SPAWN: "#FF6600",
	FRONTARC: "#FFFFFF",
	REARARC: "#AAAAAA",
	ALLY: "#99FF66",
	ALLYBLACK: "#103300",
	FOE: "#FF9966",
	FOEBLACK: "#331100",
	SELECTEDALLY: "#4ce600",
	SELECTEDFOE: "#e64c00",
	MOVEMENTRANGE: "#00ffff",
	ATTACKRANGE: "#ffff00",
	DIRECTION: "#ff00ff",
	ROTATION: "#FF0000",
	ACTIONTOKEN: "#ffcc00"
};

var ABILITYCOLOR = {
	red: {
		background: "#FF0000",
		text: "#FFFFFF"
   },
	green: {
		background: "#00FF00",
		text: "#000000"
   },
	blue: {
		background: "#0000FF",
		text: "#FFFFFF"
   },
	purple: {
		background: "#CC0066",
		text: "#FFFFFF"
   },
	yellow: {
		background: "#FFFF00",
		text: "#000000"
   },
	orange: {
		background: "#FF6600",
		text: "#000000"
   },
	brown: {
		background: "#CC6600",
		text: "#FFFFFF"
   },
	gray: {
		background: "#a6a6a6",
		text: "#FFFFFF"
   },
	black: {
		background: "#000000",
		text: "#FFFFFF"
   }
};

var ATTRIBUTES = {
	"movementrange": "movementrange",
	"attack": "attack",
	"defence": "defence",
	"damage": "damage"
};

var ATTRBONUS = {
	"movementrange": "movementboni",
	"attack": "attackboni",
	"defence": "defenceboni",
	"damage": "damageboni"
};

var ATTRDIRECTION = {
	"movementrange": "↖",
	"attack": "←",
	"defence": "↙",
	"damage": "↓"
};

var PREDEFINEDARMIES = {
	orc: [
		"basic_085",
		"basic_085",
		"basic_123",
		"darkriders_034",
		"darkriders_034",
		"omens_077",
		"omens_090",
		"sorcery_078"
	],
	empire: [
		"basic_117",
		"nexus_001",
		"sorcery_001",
		"sorcery_001",
		"sorcery_002",
		"sorcery_002",
		"sorcery_015",
		"sorcery_015"
	]
};
