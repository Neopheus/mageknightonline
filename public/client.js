//if some browser (IE!!!) doesn't support Math.hypot...
if(typeof Math.hypot != "function"){
	Math.hypot = function(x, y){
		return Math.sqrt(x*x + y*y);
	}
}
//and another unsupported function...
if(typeof String.prototype.includes != "function"){
	String.prototype.includes = function(substring){
		if(this.indexOf(substring)!=-1){
			return true;
		} else {
			return false;
		}
	}
}

var DEBUG = false;
var LOBBY = 0;
var gc, gcc;
var socket;

$(document).ready(function(){

	if(DEBUG){
		//######################################
		//only for canvas testing
		$("#uiDiv").hide();
		$("#gameDiv").show();
		PLAYER.id = "testB";
		OPPONENT.id = "testA";
		GAMESTATE.activePlayer = PLAYER.id;
		GAMESTATE.inactivePlayer = OPPONENT.id;
		moveCameraToInitialPosition();
		moveWarriorsToInitialPosition();
		//#####################################
	}

	gc = $("#gameCanvas")[0];
	gcc = gc.getContext("2d");

	socket = io.connect();

	socket.on("connect", function(){
		socket.emit("joinSite", localStorage.getItem("userUID"));
	});

	socket.on("connected", function(msg, id){
		localStorage.setItem("userUID", id);
		socket.emit("refreshAllGames");
		console.log(msg);
	});

	//#############################
	//### remote eventlisteners ###
	//#############################

	//newMessage
	socket.on("newMessage", function(msg){
		newMessage(msg);
	});

	//updatePlayer
	socket.on("updatePlayer", function(player){
		PLAYER = player;
	});

	//thatsMe
	socket.on("updateOpponent", function(opponent){
		OPPONENT = opponent;
	});

	//addNewGame
	socket.on("addNewGame", function(game){
		addNewGameToList(game);
	});

	//updateGamesList
	socket.on("updateGamesList", function(games){
		$("#listedGames").empty();
		for(var elem in games){
			if(games.hasOwnProperty(elem)){
				addNewGameToList(games[elem]);
			}
		}
	});

	//joinSucceed
	socket.on("joinSucceed", function(gameId, seed){
		if(gameId!=0){
			$("#lobbyUi").hide();
			$("#armySelectUi").show();
			setSelectArmyHeights();
			if(warriorDB==null){
				socket.emit("getWarriorDB");
			}
		} else {
			$("#gameDiv").hide();
			$("#gameUi").hide();
			$("#armySelectUi").hide();
			$("#lobbyUi").show();
		}
		setRandomSeed(seed);
		random = new Math.seedrandom(seed);
		if(DEBUG){
			$("#gameDiv").show();
			placeWarriors = true;
			setInterval(updateCanvas, INTERVAL);
		}
	});

	//getWarriorDB
	socket.on("getWarriorDB", function(db){
		warriorDB = db;
		$("#listedWarriors").empty();
		for(var warrior in warriorDB){
			if(warriorDB.hasOwnProperty(warrior)){
				addWarriorToList(warriorDB[warrior]);
			}
		}
	});

	//initializeGame
	socket.on("initializeGame", function(gamestate){
		socket.emit("updateOpponent");
		GAMESTATE = gamestate;
		placeWarriors = true;
		updateUI();
		if(rollDice(1)<=3){
			GAMESTATE.activePlayer = GAMESTATE.p1;
			GAMESTATE.inactivePlayer = GAMESTATE.p2;
		} else {
			GAMESTATE.activePlayer = GAMESTATE.p2;
			GAMESTATE.inactivePlayer = GAMESTATE.p1;
		}
		$("#armySelectUi").hide();
		$("#gameDiv").show();
		moveCameraToInitialPosition();
		moveWarriorsToInitialPosition();
		setInterval(updateCanvas, INTERVAL);
		if(PLAYER.isAI){
			ai();
			$("#gameCanvas").bind("mouseup", mouseDownCamera);
			$("#gameCanvas").bind("mousedown", mouseDownCamera);
		} else {
			$("#gameCanvas").bind("mousedown", mouseDown);
			$("#gameCanvas").bind("mouseup", mouseUp);
		}
		$("#gameCanvas").bind("mousemove", mouseMoveTooltip);
	});

	//startGame
	socket.on("startGame", function(armiesPositions){
		setArmyPositions(armiesPositions[OPPONENT.id]);
		placeWarriors = false;
		PLAYER.ARMYSIZE = getRemainingArmySize(PLAYER.id);
		OPPONENT.ARMYSIZE = getRemainingArmySize(OPPONENT.id);
		newMessage(MESSAGE.STARTGAME(yourTurn()));
		if(MAKESCREENSHOTS) downloadScreenshot("mko_screenshot_"+GAMESTATE.seed+"_"+GAMESTATE.turn+"_(initial)");
		nextTurn();
		ai();
	});

	//callMoveTo
	socket.on("callMoveTo", function(warrior, newPosition, newBaseContacts){
		warrior = getWarrior(warrior.owner, warrior.index);
		moveTo(warrior, newPosition, newBaseContacts);
		ai();
	});

	//callBreakFree
	socket.on("callBreakFree", function(warrior){
		warrior = getWarrior(warrior.owner, warrior.index);
		breakFree(warrior);
		ai();
	});

	//callFightClose
	socket.on("callFightClose", function(attacker, defender){
		attacker = getWarrior(attacker.owner, attacker.index);
		defender = getWarrior(defender.owner, defender.index);
		fightClose(attacker, defender);
		ai();
	});

	//callFightRange
	socket.on("callFightRange", function(attacker, defenders){
		attacker = getWarrior(attacker.owner, attacker.index);
		fightRange(attacker, defenders);
		ai();
	});

	//abortSpins
	socket.on("abortSpins", function(){
		abortSpins();
		ai();
	});

	//callEndTurn
	socket.on("callEndTurn", function(){
		endTurn();
		ai();
	});

	//############################
	//### local eventlisteners ###
	//############################

	$(".button").hover(function(){
		if(!$(this).hasClass("bDisabled")){
			$(this).toggleClass("bHovered");
		} else {
			$(this).removeClass("bHovered");
		}
	});

	$(".bAction").click(function(){
		if($(this).hasClass("bSelected")){
			$(this).removeClass("bSelected");
			$(this).addClass("bUnselected");
		} else {
			$(".bAction").removeClass("bSelected");
			$(".bAction").addClass("bUnselected");
			$(this).removeClass("bUnselected");
			$(this).addClass("bSelected");
		}
	});

	$("#giveMovementAction").click(function(){
		if(giveMovementAction==false){
			giveAttackAction = false;
			emptyArray(fightableWarriors);
			giveMovementAction = true;
			getMovableWarriors();
		} else {
			giveMovementAction = false;
			emptyArray(movableWarriors);
		}

	});

	$("#giveAttackAction").click(function(){
		if(giveAttackAction==false){
			giveMovementAction = false;
			emptyArray(movableWarriors);
			giveAttackAction = true;
			getFightableWarriors();
		} else {
			giveAttackAction = false;
			emptyArray(fightableWarriors);
		}

	});

	$("#setName").click(function(){
		var name = $("#username").val();
		socket.emit("setUsername", name);
		$("#username").val("");
	});

	$("#newGame").click(function(){
		var name = $("#gameName").val();
		var pw = $("#password").val();
		socket.emit("addNewGame", {name: name, pw: pw});
		$("#gameName").val("");
		$("#password").val("");
	});

	$("#listedGames").on("click", ".joinGameWithoutPW", function(){
		socket.emit("joinGame", $(this).attr("id"), null);
	}).on("click", ".joinGameWithPW", function(){
		var game = $(this).attr("id");
		prompt("Enter password:", function(input){
			socket.emit("joinGame", game, input);
		}, true);
	}).on("mouseover", "tr", function(){
		$(this).addClass("selectedRow");
	}).on("mouseout", "tr", function(){
		$(this).removeClass("selectedRow");
	});

	$("#listedWarriors").on("mouseover", "tr", function(){
		$(this).addClass("selectedRow");
	}).on("mouseout", "tr", function(){
		$(this).removeClass("selectedRow");
	}).on("click", ".addWarrior", function(){
		addToArmy($(this).attr("id"));
	}).on("click", ".base", function(){
		showBase($(this).attr("warriorId"));
	});

	$("#listedArmy").on("mouseover", "tr", function(){
		$(this).addClass("selectedRow");
	}).on("mouseout", "tr", function(){
		$(this).removeClass("selectedRow");
	}).on("click", ".removeWarrior", function(){
		removeFromArmy($(this).closest("tr").prevAll().length);
	}).on("click", ".base", function(){
		showBase($(this).attr("warriorId"));
	});

	$("#armySearchInput, #warriorSearchInput").on("change keyup paste", function(){
		var searchTerm = $(this).val().toLowerCase();
		var searchIn = "";
		if(searchTerm!=lastSearchTerm){
			if($(this).attr("id")=="armySearchInput"){
				searchIn = "#listedArmy";
			} else if($(this).attr("id")=="warriorSearchInput"){
				searchIn = "#listedWarriors";
			}
			$(searchIn).find("tr").each(function(){
				var containsTerm = false;
				$(this).find("td").each(function(){
					if($(this).html().toLowerCase().includes(searchTerm)){
						containsTerm = true;
						return false;
					}
				});
				if(containsTerm){
					$(this).show();
				} else {
					$(this).hide();
				}
			});
		}
		lastSearchTerm = searchTerm;
	});

	$("#saveArmy").click(function(){
		console.log(ARMY);
		localStorage.setItem("ARMY", JSON.stringify(ARMY));
		newMessage("Current army saved.");
	});

	$("#loadArmy").click(function(){
		var army = JSON.parse(localStorage.getItem("ARMY"));
		if(army){
			addArmy(army);
			newMessage("Saved army loaded.");
		} else {
			newMessage("No saved army found.");
		}
	});

	$(".addArmy").click(function(){
		var army = $(this).attr("army");
		addArmy(PREDEFINEDARMIES[army]);
		newMessage("Loaded predefined army '"+army+"'");
	});

	$("#ready").click(function(){
		var currentArmySize = getArmySize();
		if(currentArmySize==0){
			newMessage(MESSAGE.ARMYEMPTY);
		} else {
			var leftPoints = GAME.MAXARMYSIZE-currentArmySize;
			var question = "Set the current army? "+leftPoints+" armypoints left.";
			confirm(question, function(){
					var playAsAI = $("#playAsAI").val();
					playAsAI = (playAsAI==="false") ? false : playAsAI;
					socket.emit("setArmy", ARMY, playAsAI);
					socket.emit("playerReady");
					$("#playAsAI").prop("disabled", true);
					$("#ready").prop("disabled", true);
					$("#leaveGame").prop("disabled", true);
			});
		}
	});

	$("#leaveGame").click(function(){
		socket.emit("joinGame", LOBBY, null);
	});

	$("#placedButton").click(function(){
		if(validateSpawnPositions(PLAYER.id)){
			var question = "Finished placing warriors?";
			confirm(question, function(){
					socket.emit("sendOwnArmyPositions", getArmyPositions(PLAYER.id));
					$("#placedButton").hide();
			});
		} else {
			newMessage("<font color='#FF0000'>At least one of your warriors is not placed in the spawnzone!</font>");
		}
	});

	$("#abortSpinsButton").click(function(){
		question = "Cancle remaining spins?"
		confirm(question, function(){
				socket.emit("abortSpins");
		});
	});

	$("#warriorOverviews").on("click", ".attackClose", function(){
		var attacker = selectedWarrior;
		var defender = getWarrior($(this).attr("owner"), $(this).attr("index"));
		question = "Attack "+defender.name+" with "+attacker.name+"?";
		confirm(question, function(){
				socket.emit("callFightClose", compressWarrior(attacker), compressWarrior(defender));
		});
	});

	$("#warriorOverviews").on("click", ".attackRange", function(){
		var attacker = selectedWarrior;
		var defenders = [];
		defenders.push(compressWarrior(getWarrior($(this).attr("owner"), $(this).attr("index"))));
		question = "Attack "+defenders[0].name+" with "+attacker.name+"?";
		confirm(question, function(){
				socket.emit("callFightRange", compressWarrior(attacker), defenders);
		});
	});

	$("#warriorOverviews").on("click", "#breakFree", function(){
		question = "Should "+selectedWarrior.name+" try to break free?";
		confirm(question, function(){
				socket.emit("callBreakFree", compressWarrior(selectedWarrior));
		});
	});

	$("#endTurnButton").click(function(){
		if(!$(this).hasClass("bDisabled")){
			socket.emit("callEndTurn");
		}
	});

	$("#screenshotButton").click(function(){
		downloadScreenshot("mko_screenshot_"+GAMESTATE.seed+"_inTurn"+GAMESTATE.turn+"_("+GAMESTATE.activePlayer+")_"+Date.now());
	});

});
