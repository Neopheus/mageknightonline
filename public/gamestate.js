var GAMESTATE = null;

GAMESTATE = {
	seed: null,
	turn: 1,
	p1: "testA",
	p2: "testB",
	placeWarriors: true,
	activePlayer: null,
	inactivePlayer: null,
	currentPhase: null,
	executedActions: 0,
	armies: {
		testA: {},
		testB: {}
	},
	controlpoints:{
		cp1: {position: {x: 500, y: 500}, radius: 37, ownership: null, baseContacts: []},
		cp2: {position: {x: 1000, y: 1000}, radius: 37, ownership: null, baseContacts: []},
		cp3: {position: {x: 1500, y: 1500}, radius: 37, ownership: null, baseContacts: []}
	},
	tablestate: {}
}

GAMESTATE["armies"]["testA"]["0"] = {
	name: "a",
	index: 0,
	owner: "testA",
	baseradius:35,
	frontarc: 3,
	reararc: 3,
	range: 7,
	baseContacts: [],
	newBaseContacts: [],
	position:{
		x: 200,
		y:200,
		rotation: 0,
		direction: {
			x: 1,
			y: 0
		}
	},
	drawPosition:{
		x: 200,
		y: 200,
		rotation: 0,
		direction: {
			x: 1,
			y: 0
		}
	},
	activeState: {
		marker: "green",
		movementrange: 8,
		movementboni: [],
		attack: 8,
		attackboni: [],
		defence: 16,
		defenceboni: ["orangeSquare"],
		damage: 2,
		damageboni: []
	},
	movementtype: "shoe",
	attacktype: "sword",
	attackbonus: 0,
	defensetype: "normal",
	damagetype: "normal",
	rangeattacks: 1,
	rangedamage: 0,
	life: 8,
	damageTaken: 0,
	actions: 0,
	gotAction: false
};
GAMESTATE["armies"]["testA"]["1"] = {
	name: "b",
	index: 1,
	owner: "testA",
	baseradius:35,
	frontarc: 3,
	reararc: 3,
	range: 0,
	baseContacts: [],
	newBaseContacts: [],
	position:{
		x: 200,
		y:270,
		rotation: 0,
		direction: {
			x: 1,
			y: 0
		}
	},
	drawPosition:{
		x: 200,
		y: 270,
		rotation: 0,
		direction: {
			x: 1,
			y: 0
		}
	},
	activeState: {
		marker: "green",
		movementrange: 8,
		movementboni: [],
		attack: 8,
		attackboni: [],
		defence: 16,
		defenceboni: ["orangeSquare"],
		damage: 2,
		damageboni: []
	},
	movementtype: "shoe",
	attacktype: "sword",
	attackbonus: 0,
	defensetype: "normal",
	damagetype: "normal",
	rangeattacks: 1,
	rangedamage: 0,
	life: 8,
	damageTaken: 0,
	actions: 0,
	gotAction: false
};
//############################################################################################
GAMESTATE["armies"]["testB"]["0"] = {
	name: "c",
	index: 0,
	owner: "testB",
	baseradius:35,
	frontarc: 3,
	reararc: 3,
	range: 3,
	baseContacts: [],
	newBaseContacts: [],
	position:{
		x: 400,
		y:400,
		rotation: 0,
		direction: {
			x: 1,
			y: 0
		}
	},
	drawPosition:{
		x: 400,
		y: 400,
		rotation: 0,
		direction: {
			x: 1,
			y: 0
		}
	},
	activeState: {
		marker: "green",
		movementrange: 8,
		movementboni: [],
		attack: 8,
		attackboni: [],
		defence: 16,
		defenceboni: ["orangeSquare"],
		damage: 2,
		damageboni: []
	},
	movementtype: "shoe",
	attacktype: "sword",
	attackbonus: 0,
	defensetype: "normal",
	damagetype: "normal",
	rangeattacks: 1,
	rangedamage: 0,
	life: 8,
	damageTaken: 0,
	actions: 0,
	gotAction: false
};
GAMESTATE["armies"]["testB"]["1"] = {
	name: "c",
	index: 1,
	owner: "testB",
	baseradius:35,
	frontarc: 3,
	reararc: 3,
	range: 3,
	baseContacts: [],
	newBaseContacts: [],
	position:{
		x: 400,
		y:400,
		rotation: 0,
		direction: {
			x: 1,
			y: 0
		}
	},
	drawPosition:{
		x: 400,
		y: 400,
		rotation: 0,
		direction: {
			x: 1,
			y: 0
		}
	},
	activeState: {
		marker: "green",
		movementrange: 8,
		movementboni: [],
		attack: 8,
		attackboni: [],
		defence: 16,
		defenceboni: ["orangeSquare"],
		damage: 2,
		damageboni: []
	},
	movementtype: "shoe",
	attacktype: "sword",
	attackbonus: 0,
	defensetype: "normal",
	damagetype: "normal",
	rangeattacks: 1,
	rangedamage: 0,
	life: 8,
	damageTaken: 0,
	actions: 0,
	gotAction: false
};
