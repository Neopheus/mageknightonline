var MAXTIME = 30*1000;
var POSSIBLEACTIONS = 5;
var MAXTURNS = 4;

function mcts(){
  console.log("### START MCTS ###");
  startTimer("MCTS");
  var tree = createTree(GAMESTATE);
  var iteration = 0;
  while(!tree.solutionFound && tree.elapsedTime<MAXTIME){
    //startTimer("---Iteration---"+iteration);
    var selectedLeaf = tree.selection();
    if(selectedLeaf.n!=0){
      selectedLeaf.expand();
      selectedLeaf = selectedLeaf.childs[0];
    }
    var value = selectedLeaf.simulate();
    selectedLeaf.backpropagation(value);
    tree.elapsedTime += Date.now()-tree.lastTime;
    tree.lastTime = Date.now();
    //stopTimer("---Iteration---"+iteration);
    iteration++
  }
  stopTimer("MCTS");
  console.log("iterations: ", iteration-1);
  console.log("### END MCTS ###");
  console.log(tree);
  return tree.root.getBestChild().actions;
}

function createTree(gs){
  var tree = {
    root: createNode(gs, null, null),
    elapsedTime: 0,
    lastTime: Date.now(),
    solutionFound: false,
    selection: function(){
      var bestLeaf = this.root.getBestChild();
      while(bestLeaf.childs!=0){
        bestLeaf = bestLeaf.getBestChild();
      }
      return bestLeaf;
    }
  }
  tree.root.expand();
  return tree;
}

function createNode(state, parent, actions){
  var node = {
    state: state,
    parent: parent,
    actions: actions,
    childs: [],
    root: null,
    w: 0,
    n: 0,
    ucb1: 0,
    calculateUCB1: function(){
      this.ucb1 = (this.n===0) ? Infinity : this.w/this.n+2*Math.sqrt(Math.log(this.root.n)/this.n);
    },
    expand: function(){
      //startTimer("expansion")
      var allPossibleActions = createAllPossibleActions(this.state);
      for(var i=0; i<allPossibleActions.length; i++){
        var node = createNode(null, this, allPossibleActions[i])
        node.calculateState();
        this.childs.push(node);
      }
      //stopTimer("expansion");
    },
    calculateState: function(){
      //startTimer("calculateState");
      this.state = calculateState(this.parent.state, this.actions);
      if(this.state.winner===PLAYER.id) this.root.solutionFound = true;
      //stopTimer("calculateState");
    },
    simulate: function(){
      //startTimer("simulation");
      var value = simulateGame(this.state);
      //stopTimer("simulation");
      return value;
    },
    backpropagation: function(value){
      this.w+=value;
      this.n++;
      if(this.parent) this.parent.backpropagation(value);
    },
    getBestChild: function(){
      var bestChild = null;
      var bestUCB1 = 0;
      for(var i=0; i<this.childs.length; i++){
        this.childs[i].calculateUCB1();
        if(this.childs[i].ucb1>bestUCB1){
          bestChild = this.childs[i];
          bestUCB1 = this.childs[i].ucb1;
        }
      }
      return bestChild;
    },
    getRoot: function(){
      if(parent){
        return this.parent;
      } else {
        return this;
      }
    },
    getLeafs: function(){
      var leafs = [];
      if(this.childs.length!=0){
        for(var i=0; i<this.childs.length; i++){
          leafs.push(this.childs[i].getLeafs);
        }
      } else {
        leafs.push(this);
      }
      return leafs;
    }
  };
  node.root = node.getRoot();
  return node;
}

function createAllPossibleActions(state){
  var allActions = [];
  //TODO create all possible combinations of the scripts and permutations of those combinations
  //not possible, would be to many actions
  for(var i=0; i<POSSIBLEACTIONS; i++){
    var act = randomReasoning();
    allActions.push(act);
  }
  return allActions;
}

function calculateState(state, act){
  var calculatedState;
  saveGamestate();
  SIMULATING = true;
  GAMESTATE = cloneObject(state);
  actions = cloneProperties(act);
  //console.log(GAMESTATE);
  //console.log(act);
  ai();
  //console.log(GAMESTATE);
  calculatedState = cloneObject(GAMESTATE);
  SIMULATING = false;
  restoreGamestate();
  return calculatedState;
}

function simulateGame(gs){
  var value = 0;
  saveGamestate();
  GAMESTATE = cloneObject(gs);
  saveUser();
  if(GAMESTATE.activePlayer!=PLAYER.id) swapUser();
  SIMULATINGGAME = 1;
  SIMULATING = true;
  var startTurn = GAMESTATE.turn;
  ai();
  console.log("simulatedTurns",GAMESTATE.turn-startTurn);
  SIMULATING = false;
  SIMULATINGGAME = false;
  restoreUser();
  //value = (GAMESTATE.winner===PLAYER.id) ? 1 : 0;
  value = calculateFitness(GAMESTATE);
  restoreGamestate();
  return value;
}
