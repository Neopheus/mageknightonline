   ###############################
   ### MageKnight Online (MKO) ###
   ###############################

Author:
Bernhard Maier (mail@neopheus.de)

Platform:
Browsergame

About:
This is a small project to implement a browsergame version of the
tabletop game Mage Knight from WizKids. Two reasons were responsible
for me to do this project: On the one hand Mage Knight isn't available
any more (for a long time) and on the other hand i needed a project to
learn a bit more about webapplications built with Node.js and MongoDB.
