var fs = require("fs");

//no usaged for
function writeWarriorToDB(warrior){
	fs.writeFileSync("./warriors/warriorDB/"+warrior.id+" - "+warrior.name+".MKOC", JSON.stringify(warrior), "utf8");
}

function readWarriorFromDB(filename){
	return JSON.parse(fs.readFileSync("./warriors/warriorDB/"+filename, "utf8"));
}

function importAllWarriors(){
	var warriors = {};
	var filenames = fs.readdirSync("./warriors/warriorDB/");

	filenames.forEach(function(filename){
		warrior = readWarriorFromDB(filename)
		if(warrior.equippable){
			warrior.baseradius = 39;
		} else {
			warrior.baseradius = 35;
		}
		warrior.position = {
			x: 0,
			y: 0,
			rotation: 0,
			direction: {
				x: 1,
				y: 0
			}
		};
		warrior.drawPosition = {
			x: 0,
			y: 0,
			rotation: 0,
			direction: {
				x: 1,
				y: 0
			}
		};
		warrior.oldPosition = {
			x: 0,
			y: 0,
			rotation: 0,
			direction: {
				x: 1,
				y: 0
			}
		};
		warrior.baseContacts = [];
		warrior.newBaseContacts = [];
		warrior.rangeTargets = [];
		warrior.damageTaken = 0;
		warrior.activeState = warrior.state[warrior.damageTaken];
		warrior.actions = 0;
		warrior.gotAction = false;
		warrior.breakFreeFailed = null;
		warrior.index = null;
		warrior.owner = null;
		warriors[warrior.id] = warrior;
	});

	return warriors;
}


//no usage for
function pickWarrior(warrior){
	var clone;
	if(warrior instanceof Array){
		clone = [];
		for(var i=0; i<warrior.length; i++){
			clone[i] = pickWarrior(warrior[i]);
		}
		return clone;
	} else if(warrior instanceof Object){
		clone = {};
		for(var attr in warrior){
			clone[attr] = pickWarrior(warrior[attr]);
		}
		return clone;
	} else {
		return warrior;
	}
}

exports.writeWarriorToDB = writeWarriorToDB;
exports.readWarriorFromDB = readWarriorFromDB;
exports.importAllWarriors = importAllWarriors;
exports.pickWarrior = pickWarrior;
