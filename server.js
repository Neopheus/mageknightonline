var express = require("express");
var app = express();
var server = require("http").createServer(app);
var io = require("socket.io").listen(server);
var config = require("./config.json");

var wi = require("./warriors/warriorImport.js");

var warriorDB = wi.importAllWarriors();

server.listen(config.port);
app.use(express.static(__dirname + "/public"));
app.get("/", function(req, res){
	res.sendFile(__dirname + "/public/index.html");
});

var LOBBY = 0;
var players = {};
var games = {};
games[LOBBY] = {name: "Lobby", id: LOBBY, pw: null, seed: null, joinedPlayers: [], gamestate: null};



io.sockets.on("connection", function(socket){

	var player = {};



	//#######################
	//### event listeners ###
	//#######################

	socket.on("disconnect", function(){
		console.log("disconnect...");
		player.disconnected = true;
		setTimeout(function(){
			if(player.disconnected && (typeof games[player.game] != "undefined")){
				removePlayerFromGame();
				updateGamesList();
				delete players[player.id];
				console.log("...disconnected!");
			} else {
				//TODO handle reconnect like load gamestate, messages and so on
				console.log("...reconnected!");
			}
		}, config.TIMEOUTSECS*1000);
	});

	socket.on("joinSite", function(uid){
		if(uid!=null && players.hasOwnProperty(uid)){
			player = players[uid];
			player.disconnected = false;
			if(!games.hasOwnProperty(player.game)){
				joinGame(LOBBY);
			} else {
				//TODO has to be a change to an exisitng game (switch room (ui), reload gamestate, etc.).
				//may could extense joinGame() with if(initialJoin) or something else
				//currently it works only for refreshes and not for reconnects to running games, it's just an ui change
				socket.emit("joinSucceed", player.game, games[player.game].seed);
			}
		} else {
			player.id = newId();
			player.name = "user." + player.id;
			player.isAI = null;
			addPlayerToGame(LOBBY);
			players[player.id] = player;
			socket.emit("connected", "You're now connected to " + player.game + " as " + player.name + " with ip " + socket.request.connection.remoteAddress, player.id);
			lobbyMessage(player.name + " joined the Lobby.");
			clientMessage("Welcome to MageKnightOnline.");
		}
		updatePlayer();
		updateGamesList();
	});

	socket.on("setUsername", function(name){
		if(name=="") name = "DefaultUserName";
		player.name = name;
		clientMessage("Username is set to \""+player.name+"\".");
		updatePlayer();
	});

	socket.on("addNewGame", function(game){
		game.id = newId();
		game.seed = (game.name=="") ? game.id : game.name;
		game.name = (game.name=="") ? "NoGameName" : game.name;
		game.pw = (game.pw=="") ? null : game.pw;
		game.p1 = null;
		game.p2 = null;
		game.joinedPlayers = [];
		game.gamestate = createInitialGamestate(game);
		game.armiesPosition = {};
		game.compressed = compressGame(game);
		games[game.id] = game;
		io.to(LOBBY).emit("addNewGame", game.compressed);
		joinGame(game.id);
	});

	socket.on("joinGame", function(game, pw){
		if(pw===games[game].pw){
			joinGame(game);
		} else {
			clientMessage("Wrong password entered!");
		}
	});

	socket.on("getWarriorDB", function(){
		socket.emit("getWarriorDB", warriorDB);
	});

	socket.on("setArmy", function(army, playAsAI){
		games[player.game]["gamestate"]["armies"][player.id] = {};
		for(var i=0; i<army.length; i++){
			var warrior = wi.pickWarrior(warriorDB[army[i]]);
			warrior.index = i;
			warrior.owner = player.id;
			games[player.game]["gamestate"]["armies"][player.id][i] = warrior;
		}
		player.isAI = playAsAI;
		updatePlayer();
	});

	socket.on("playerReady", function(){
		player.ready = true;
		if(allPlayersReady(player.game)){
			setAllPlayersNotReady(player.game);
			clientMessage("Your army is set.");
			gameMessage("All armies set, place your warriors.", "all");
			initializeGame();
		} else {
			clientMessage("Your army is set, just wait for the other player.");
			gameMessage(player.name+" is ready and waits for you.", "others");
			updatePlayer();
		}
	});

	socket.on("updateOpponent", function(){
		updateOpponent();
	});

	socket.on("sendOwnArmyPositions", function(positions){
		player.ready = true;
		games[player.game]["armiesPosition"][player.id] = positions;
		if(allPlayersReady(player.game)){
			setAllPlayersNotReady(player.game);
			clientMessage("Your warriors are placed.");
			gameMessage("Armies are placed and game starts.", "all");
			startGame();
		} else {
			clientMessage("Your warriors are placed, just wait for the other player.");
			gameMessage(player.name+" is ready and waits for you.", "others");
			updatePlayer();
		}
	});

	socket.on("callMoveTo", function(warrior, newPosition, newBaseContacts){
		io.to(player.game).emit("callMoveTo", warrior, newPosition, newBaseContacts);
	});

	socket.on("callBreakFree", function(warrior){
		io.to(player.game).emit("callBreakFree", warrior);
	});

	socket.on("callFightClose", function(attacker, defender){
		io.to(player.game).emit("callFightClose", attacker, defender);
	});

	socket.on("callFightRange", function(attacker, defenders){
		io.to(player.game).emit("callFightRange", attacker, defenders);
	});

	socket.on("abortSpins", function(){
		io.to(player.game).emit("abortSpins");
	});

	socket.on("callEndTurn", function(){
		io.to(player.game).emit("callEndTurn");
	});

	socket.on("callEndGame", function(){
		io.to(player.game).emit("callEndGame");
	});

	socket.on("aiStillAlive", function(){
		io.to(player.game).emit("aiStillAlive");
	});



	//#################
	//### functions ###
	//#################

	function clientMessage(msg){
		socket.emit("newMessage", msg);
	}

	function gameMessage(msg, to){
		if(player.game!=LOBBY){
			if(to == "others"){
				socket.broadcast.to(player.game).emit("newMessage", msg);
			} else if(to == "all") {
				io.to(player.game).emit("newMessage", msg);
			}
		}
	}

	function lobbyMessage(msg){
		io.to(LOBBY).emit("newMessage", msg);
	}

	function updatePlayer(){
		socket.emit("updatePlayer", player);
	}

	function updateOpponent(){
		socket.broadcast.to(player.game).emit("updateOpponent", player);
	}

	function compressGame(game){
		var hasPW = false;
		if(game.pw!=null) hasPW = true;
		var compressedGame = {
			id: game.id,
			name: game.name,
			pw: hasPW,
			joinedPlayers: game.joinedPlayers.length
		};
		return compressedGame;
	}

	function compressGames(){
		var compressedGames = [];
		for(var game in games){
			if(game!=LOBBY) compressedGames.push(games[game].compressed);
		}
		return compressedGames;
	}

	function joinGame(game){
		if(games[game].joinedPlayers.length<config.MAXPLAYERS || game==LOBBY){
			removePlayerFromGame();
			addPlayerToGame(game);
			updatePlayer();
			updateGamesList();
			if(game==LOBBY){
				clientMessage("Welcome to Lobby");
			} else {
				clientMessage("Welcome to game \""+games[game].name+"\".");
			}
		} else {
			clientMessage("Couldn't join: full or already in.")
		}
	}

	function removePlayerFromGame(){
		var index = games[player.game].joinedPlayers.indexOf(player.id);
		if(index>-1){
			games[player.game].joinedPlayers.splice(index, 1);
			if(player.game!=LOBBY){
				if(games[player.game].joinedPlayers.length<=0){
					delete games[player.game];
				} else {
					if(games[player.game].p1 == player.id){
						games[player.game].p1 = null;
					} else {
						games[player.game].p2 = null;
					}
					gameMessage(player.name+" has left the game!", "others");
					games[player.game].compressed = compressGame(games[player.game]);
				}
			}
			socket.leave(player.game);
			player.game = null;
		}
	}

	function addPlayerToGame(game){
		player.ready = false;
		player.game = game;
		games[player.game].joinedPlayers.push(player.id);
		if(game!=LOBBY){
			if(games[game].p1 == null){
				games[game].p1 = player.id;
			} else {
				games[game].p2 = player.id;
			}
		}
		socket.join(player.game);
		games[game].compressed = compressGame(games[game]);
		socket.emit("joinSucceed", player.game, games[player.game].seed);
		gameMessage(player.name+" has joined the game!", "others");
	}

	function updateGamesList(){
		io.to(LOBBY).emit("updateGamesList", compressGames());
	}

	function allPlayersReady(game){
		if(games[game].joinedPlayers.length==config.MAXPLAYERS){
			for(var i=0; i<games[game].joinedPlayers.length; i++){
				if(!players[games[game].joinedPlayers[i]].ready){
					return false;
				}
			}
			return true;
		} else {
			return false;
		}
	}

	function setAllPlayersNotReady(game){
		for(var i=0; i<games[game].joinedPlayers.length; i++){
			players[games[game].joinedPlayers[i]].ready = false;
		}
	}

	function createInitialGamestate(game){
		var gamestate = {
			seed: game.seed,
			gameover: false,
			winner: null,
			turn: 0,
			p1: null,
			p2: null,
			activePlayer: null,
			inactivePlayer: null,
			currentPhase: null,
			executedActions: 0,
			armies: {},
			controlpoints:{
				A: {name: "A", position: {x: 500, y: 500}, radius: 37, owner: null, baseContacts: []},
				B: {name: "B", position: {x: 1500, y: 500}, radius: 37, owner: null, baseContacts: []},
				C: {name: "C", position: {x: 1000, y: 1000}, radius: 37, owner: null, baseContacts: []},
				D: {name: "D", position: {x: 500, y: 1500}, radius: 37, owner: null, baseContacts: []},
				E: {name: "E", position: {x: 1500, y: 1500}, radius: 37, owner: null, baseContacts: []}
			},
			allCpHold:{
				from: null,
				rounds: null
			},
			tablestate: {}
		};
		min = -250;
		max = 250;
		for(var cp in gamestate.controlpoints){
			cp = gamestate.controlpoints[cp];
			cp.position.x+= Math.floor(Math.random() * (max - min +1)) + min;
			cp.position.y+= Math.floor(Math.random() * (max - min +1)) + min;
		}
		return gamestate;
	}

	function initializeGame(){
		games[player.game].gamestate.p1 = games[player.game].p1;
		games[player.game].gamestate.p2 = games[player.game].p2;
		io.to(player.game).emit("initializeGame", games[player.game].gamestate);
	}

	function startGame(){
		io.to(player.game).emit("startGame", games[player.game]["armiesPosition"]);
	}

});



//########################
//### helper functions ###
//########################

function newId(){
	var randomgeneratedUID = Math.random().toString(36).substring(3,16);
	return randomgeneratedUID;
}



//###################
//### END OF CODE ###
//###################

console.log("###################################################");
console.log("### Server is running on http://127.0.0.1:" + config.port + "/ ###");
console.log("###################################################");
